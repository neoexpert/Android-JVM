package com.neoexpert.jvm;

import com.neoexpert.jvm._abstract.*;
import com.neoexpert.jvm.adapter.*;
import com.neoexpert.jvm.natives.*;
import java.io.*;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

public class JVM implements OutputAdapter
{

	public static int system_thread_group;

	private static void createSystemThreadGroup()
	{
		AClass c = com.neoexpert.jvm.Class.getClass("java/lang/ThreadGroup");
		MyThread t = new MyThread(c,null, null);
		system_thread_group=t.newInstance("<init>()V");
	}
	
	public static void init()
 	{
        com.neoexpert.jvm.Class.getClass("java/lang/Math");
        com.neoexpert.jvm.Class.getClass("java/lang/Thread");
        com.neoexpert.jvm.Class.getClass("java/lang/Class");
        com.neoexpert.jvm.Class.getClass("java/lang/String");
	 	createSystemThreadGroup();

        com.neoexpert.jvm.Class.getClass("java/lang/ClassLoader");
		com.neoexpert.jvm.Class.getClass("java/util/HashMap");

		com.neoexpert.jvm.Class.getClass("java/util/Map");
		com.neoexpert.jvm.Class.getClass("java/util/AbstractMap");
		com.neoexpert.jvm.Class.getClass("java/util/HashMap$Node");
		com.neoexpert.jvm.Class.getClass("java/util/HashMap$TreeNode");
		com.neoexpert.jvm.Class.getClass("java/util/LinkedHashMap$Entry");
		com.neoexpert.jvm.Class.getClass("java/util/Map$Entry");
		com.neoexpert.jvm.Class.getClass("java/lang/Comparable");

		
		com.neoexpert.jvm.Class.getClass("java/util/Arrays");
		
		com.neoexpert.jvm.Class.getClass("sun/reflect/Reflection");
		/*
		com.neoexpert.jvm.Class.getClass("sun/reflect/ReflectionFactory");
        com.neoexpert.jvm.Class.getClass("java/lang/reflect/Field");
		com.neoexpert.jvm.Class.getClass("java/security/AccessControlContext");
		com.neoexpert.jvm.Class.getClass("sun/reflect/ReflectionFactory$GetReflectionFactoryAction");

        com.neoexpert.jvm.Class.getClass("java/lang/reflect/Modifier");

		com.neoexpert.jvm.Class.getClass("java/io/BufferedInputStream");
        com.neoexpert.jvm.Class.getClass("sun/nio/cs/StreamEncoder");
		
		initializeSystemClass();
		*/
		//com.neoexpert.jvm.Class.getClass("java/io/File");
		//com.neoexpert.jvm.Class.getClass("java/io/File$PathStatus");
		
    }
	
	public static void initializeSystemClass(){
		AClass c=com.neoexpert.jvm.Class.getClass("java/lang/System");

		//t=c.createInitMethod();
		MyThread t=new MyThread(c, "main", null);
		//t.setMethodToRun("main([Ljava/lang/String;)V");




		t.setMethodToRun("initializeSystemClass()V");
		t.run();
	}
    @Override
    public void jvmdebug(String a) {
        // TODO: Implement this method
    }


    public static void main(String[] args) throws Exception {

		if (args != null)
		{
			String s = new File("cp/").getAbsolutePath();
			Class.classpath = s;
			Class.addJar("rt.jar");
			Class.addJar("charsets.jar");
		}
        Class.reset();
		MyThread.reset();
		init();
        MyThread mt = null;






        AClass thc=com.neoexpert.jvm.Class.getClass("java/lang/Thread");
        AClass c = com.neoexpert.jvm.Class.getClass("java/lang/ThreadGroup");


        //com.neoexpert.jvm.Class.getClass("sun/misc/Unsafe");
        c=com.neoexpert.jvm.Class.getClass("java/lang/System");
        //com.neoexpert.jvm.Class.getClass("java/io/BufferedInputStream");
        //com.neoexpert.jvm.Class.getClass("java/lang/ref/Reference");
        //com.neoexpert.jvm.Class.getClass("sun/reflect/generics/repository/ClassRepository");

        //c=com.neoexpert.jvm.Class.getClass("java/lang/Math");
        //classa.setClass(c);

        //if (c.hasMainMethod())
        {
            MyThread t=new MyThread(c,"main", null);
            try
            {


            }
            catch (Exception e)
            {
                t.getCurrentFrame();
                throw new RuntimeException(e);
            }

        }
        //else{
        //findViewById(R.id.methodLayout).setVisibility(View.GONE);
        //log("no main method");
        //}

        Class.getClass("java/util/HashMap");
        Class.getClass("sun/reflect/Reflection");
        Class.getClass("sun/reflect/ReflectionFactory");
        Class.getClass("java/lang/reflect/Modifier");
        Class.getClass("java/lang/reflect/Field");
        Class.getClass("sun/misc/Unsafe");
        Class.getClass("sun/misc/VM");
        Class.getClass("java/util/concurrent/atomic/AtomicReferenceFieldUpdater");
        //Class.getClass("java/io/FileSystem");



        //Class.getClass("java/io/BufferedInputStream");







        //AClass cl = Class.getClass("java/lang/Class");
        //AMethod m = cl.getMyMethod("getClassLoader0()Ljava/lang/ClassLoader;");
        //m.setNative(true);

        //AClass system = Class.getClass("java/lang/System");
        //mt.setMethodToRun("initializeSystemClass()V");
        //mt.run();

         //c = Class.getClass(args[0].replaceAll("\\.", "/"));
         c=Class.getClass("java/io/Main");

        mt = new MyThread(c);

		mt.setMethodToRun("main([Ljava/lang/String;)V");
        mt.run();

    }

    @Override
    public void logln(String a, boolean debug) {
        java.lang.System.out.println(a);
    }

}
