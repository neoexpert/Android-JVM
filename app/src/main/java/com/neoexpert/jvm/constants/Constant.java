package com.neoexpert.jvm.constants;
import com.neoexpert.jvm.*;
import java.nio.*;

public class Constant implements ClassItem
{

	@Override
	public String parseModifiers()
	{
		return "";
	}
	

	public float fvalue;

	public double dvalue=Double.NaN;

	public long lvalue;

	public int ivalue;
	@Override
	public void setTag(byte tag)
	{
		this.tag=tag;
	}

	@Override
	public void setId(short id)
	{
		this.id=id;
	}
	
	
	private String toString="";
	public String str;

	private byte tag;
	public int index1=-1, index2=-1;

	private short id;
	public short getId(){
		return id;
	}
	public Constant(short id,ByteBuffer buf){
		this.id=id;
		 tag = buf.get();
		 String s;
		switch (tag)
		{
			default:
				log("unknown pool item type " + buf.get(buf.position() - 1));
				return;
			case CUtf8: 
				str=decodeString(buf);
				if(str.equals("putAll"))
					getClass();
				log(str);
				return;
			case CClass: case CString: case CMethodType:
				s = "%s ref=%d%n"; index1 = buf.getChar();
				break;
			case CFieldRef: case CMethodRef:
			case CInterfaceMethodRef: case CNameAndType:
				s = "%s ref1=%d, ref2=%d%n";
				index1 = buf.getChar(); index2 = buf.getChar();
				break;
			case CInteger: 
				ivalue=buf.getInt();
				s = "%s value=" + ivalue + "%n";
			break;
			case CFloat:
				fvalue=buf.getFloat();
				s = "%s value=" + fvalue + "%n";
				break;
			case CDouble: 
				dvalue=buf.getDouble();
				s = "%s value=" + dvalue + "%n"; 
				break;
			case CLong: 
				lvalue=buf.getLong();
				s = "%s value=" + lvalue + "%n";
				break;
			case CMethodHandle:
				s = "%s kind=%d, ref=%d%n"; index1 = buf.get(); index2 = buf.getChar();
				break;
			case CInvokeDynamic:
				s = "%s bootstrap_method_attr_index=%d, ref=%d%n";
				index1 = buf.getChar(); index2 = buf.getChar();
				break;
		}
		if(index1==184||index2==184||index1==286||index2==286)
			getClass();
		log(String.format(s, FMT[tag],index1,index2));
	}
	private static String[] FMT= {
		null, "Utf8", null, "Integer", "Float", "Long", "Double", "Class",
		"String", "Field", "Method", "Interface Method", "Name and Type",
		null, null, "MethodHandle", "MethodType", null, "InvokeDynamic"
	};

	public byte getTag()
	{
		return tag;
	}
	//byte[] d;
	private  String decodeString(ByteBuffer buf)
	{
		int size=buf.getChar(), oldLimit=buf.limit();
		buf.limit(buf.position() + size);
		StringBuilder sb=new StringBuilder(size + (size >> 1) + 16);
		while (buf.hasRemaining())
		{
			byte b=buf.get();
			if (b > 0) sb.append((char)b);
			else
			{
				int b2 = buf.get();
				if ((b & 0xf0) != 0xe0)
					sb.append((char)((b & 0x1F) << 6 | b2 & 0x3F));
				else
				{
					int b3 = buf.get();
					sb.append((char)((b & 0x0F) << 12 | (b2 & 0x3F) << 6 | b3 & 0x3F));
				}
			}
		}
		buf.limit(oldLimit);
		return(sb.toString());
	}
	private void log(String t)
	{
		toString+=t;
	}

	@Override
	public String toString()
	{
		return toString;
	}
	
}
