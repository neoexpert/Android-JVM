package com.neoexpert.jvm;

public interface ClassItem
{
	public static final byte CUtf8               = 1;
	public static final byte CInteger            = 3;
	public static final byte CFloat              = 4;
	public static final byte CLong               = 5;
	public static final byte CDouble             = 6;
	public static final byte CClass              = 7;
	public static final byte CString             = 8;
	public static final byte CFieldRef           = 9;
	public static final byte CMethodRef          =10;
	public static final byte CInterfaceMethodRef =11;
	public static final byte CNameAndType        =12;
	public static final byte CMethodHandle       =15;
	public static final byte CMethodType         =16;
	public static final byte CInvokeDynamic      =18;

	public String parseModifiers();

	
	public byte getTag();
	public void setTag(byte tag);
	public short getId();
	public void setId(short id);
}
