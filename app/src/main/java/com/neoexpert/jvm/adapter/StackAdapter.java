package com.neoexpert.jvm.adapter;

import com.neoexpert.jvm.Frame;

public interface StackAdapter {

	void setFrame(Frame peek);

	void notifyDataSetChanged();

}
