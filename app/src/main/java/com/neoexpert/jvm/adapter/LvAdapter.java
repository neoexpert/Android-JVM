package com.neoexpert.jvm.adapter;

import com.neoexpert.jvm.Frame;

public interface LvAdapter {

	void setLv(Frame peek);

	void notifyDataSetChanged();

}
