package com.neoexpert.jvm.adapter;
import java.util.*;

import com.neoexpert.jvm.Frame;

public interface FrameStackAdapter
{

	public void notifyDataSetChanged();


	public void setStack(Stack<Frame> stack);

}
