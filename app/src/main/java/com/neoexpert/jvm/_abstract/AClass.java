package com.neoexpert.jvm._abstract;
import com.neoexpert.jvm.*;
import com.neoexpert.jvm.constants.*;
import java.util.*;

public abstract class AClass
{
	public static final int ACC_PUBLIC=0x0001;//	Declared public; may be accessed from outside its package.
	public static final int ACC_FINAL=0x0010;//	Declared final; no subclasses allowed.
	public static final int ACC_SUPER=0x0020;//	Treat superclass methods specially when invoked by the invokespecial instruction.
	public static final int ACC_INTERFACE=0x0200;//	Is an interface, not a class.
	public static final int ACC_ABSTRACT=0x0400;//	Declared abstract; must not be instantiated.
	public static final int ACC_SYNTHETIC=0x1000;//	Declared synthetic; not present in the source code.
	public static final int ACC_ANNOTATION=0x2000;//	Declared as an annotation type.
	public static final int ACC_ENUM=0x4000;
	protected int access_flags;
	

	public boolean isPublic()
	{
		return (access_flags&ACC_PUBLIC)!=0;
	}
	public boolean isFinal()
	{
		return (access_flags&ACC_FINAL)!=0;
	}
	public boolean isSuper()
	{
		return (access_flags&ACC_SUPER)!=0;
	}
	public boolean isInterface()
	{
		return (access_flags&ACC_INTERFACE)!=0;
	}
	public boolean isAbstract()
	{
		return (access_flags&ACC_ABSTRACT)!=0;
	}
	public boolean isSynthetic()
	{
		return (access_flags&ACC_SYNTHETIC)!=0;
	}
	public boolean isAnnotation()
	{
		return (access_flags&ACC_ANNOTATION)!=0;
	}
	public boolean isEnum()
	{
		return (access_flags&ACC_ENUM)!=0;
	}
	public HashMap<String,AMethod> methodsbyname=new HashMap<>();
	public HashMap<String,AField> fields=new HashMap<>();

	public abstract void putStatic(String fname, Object v);

	

	public abstract void putStatic(int id, Object obj);

	public abstract boolean isInitialized();
	public MyThread createInitMethod()
	{
		// TODO: Implement this method
		return null;
	}

	public abstract void clinit();
	public abstract Constant getConstant(int i);

	public abstract boolean isInstance(Object obj);
	public abstract boolean isInstance(AClass cl);

	public abstract String getName();

	public abstract AMethod getMethod(String mname,String parameters, int id);
	public abstract Iterator<Constant> constantsIterator();

	public abstract void getStatic(String str, String desc,Frame cF);
	public abstract Object getStatic(String str, String desc);
	public abstract AMethod getMethod(int i);
	public abstract AMethod getMethod(String nameAndDesc);

	public abstract void getStatic(int i,Frame cF);
	public abstract void getConstantInstance(int id, Frame cF);
	public abstract AMethod getMainMethod();
	public abstract AMethod getCLInitMethod();
	
	public abstract AClass getClass(int id);
	public abstract AMethod getMyMethod(String nameAndDesc);
	public abstract int getFieldId(String field);
	public abstract HashMap<String,Object> getStaticFields();

	public abstract AClass getSuperClass();

	@Override
	public boolean equals(Object obj)
	{
		if(obj instanceof AClass)
			return((AClass)obj).getName().equals(getName());
		return false;
	}

    public abstract int getMyInstanceRef();
}
