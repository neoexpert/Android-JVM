package com.neoexpert.jvm._abstract;
import com.neoexpert.jvm.*;
import java.util.*;

public abstract class AMethod implements ClassItem
{
	public ArrayList<String> args;

	public abstract int getMaxLocals();
	public abstract int getMaxStack();
	public abstract boolean isStatic();
	public abstract int getCurrentLine(int pc);
	public abstract MException checkException(int pc);
	public abstract void invoke(Stack<Object> stack,MyThread t);
	public abstract int getParamsCount();
	public abstract String getName();
	public abstract String getNameAndDesc();
	public abstract boolean isAbstract();
	public abstract boolean isNative();
	public abstract AClass getMyClass();
	public abstract byte[] getCode();

	public abstract void setNative(boolean b);
}
