package com.neoexpert.jvm._abstract;
import com.neoexpert.jvm.*;

public abstract class AField implements ClassItem
{

	public abstract String getDesc();

	public abstract AClass getMyClass();

	public abstract int getModifiers();
	public abstract boolean isStatic();
	public abstract String getName();

	public abstract void setName(String p0);

	public abstract int getType();
	public abstract String parseType();
}
