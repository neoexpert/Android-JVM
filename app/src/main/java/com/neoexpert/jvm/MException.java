package com.neoexpert.jvm;
import java.nio.*;

public class MException
{
	public char start_pc;

	public char end_pc;

	public char handler_pc;

	public int catch_type;
	public MException(ByteBuffer buf){
		start_pc=buf.getChar();
		end_pc=buf.getChar();
		handler_pc=buf.getChar();
		catch_type=buf.getChar();
	}
}
