package com.neoexpert.jvm._native;
import com.neoexpert.jvm._abstract.*;
import java.lang.reflect.*;
import java.util.*;
import com.neoexpert.jvm.*;

public class NativeMethod extends AMethod
{

	@Override
	public String parseModifiers()
	{
		// TODO: Implement this method
		return "";
	}


	@Override
	public int getMaxStack()
	{
		// TODO: Implement this method
		return 0;
	}


	@Override
	public int getMaxLocals()
	{
		// TODO: Implement this method
		return 0;
	}


	@Override
	public boolean isStatic()
	{
		// TODO: Implement this method
		return false;
	}


	@Override
	public int getCurrentLine(int pc)
	{
		// TODO: Implement this method
		return 0;
	}


	@Override
	public MException checkException(int pc)
	{
		// TODO: Implement this method
		return null;
	}

	private Executable m;

	private NativeClass c;
	ArrayList<String> paramtypes;

	private String returntype;
	public NativeMethod(NativeClass c,Executable m,ArrayList<String> paramtypes,String returntype){
		this.m=m;
		this.c=c;
		this.paramtypes=paramtypes;
		this.returntype=returntype;
	}
	@Override
	public void invoke(Stack<Object> stack,MyThread t)
	{
		try
		{
			if (m instanceof Constructor)
			{
				Object[] params=new Object[paramtypes.size()];

				int j=0;
				for(int i=paramtypes.size()-1;i>=0;i--){
					String type=paramtypes.get(i);
					switch(type){
						case "I":
						case "Z":
						case "B":
						case "C":
						case "D":
						case "F":
						case "J":
						case "S":
							params[i] =stack.pop();
							break;
						default:
							params[i] =t.getFromHeap((Integer) stack.pop());
					}
					j++;
				}
				Integer addr=(Integer) stack.pop();
				ClassInstance obj=(ClassInstance)t.getFromHeap(addr);
				Object r=((Constructor)m).newInstance(params);
				obj.setNativeInstance(r);
				return;
			}
			if(m instanceof java.lang.reflect.Method){
				Object[] params=new Object[paramtypes.size()];
				
				int j=0;
				for(int i=paramtypes.size()-1;i>=0;i--){
					String type=paramtypes.get(i);
					switch(type){
						case "I":
						case "Z":
						case "B":
						case "C":
						case "D":
						case "F":
						case "J":
						case "S":
							params[i] =stack.pop();
							break;
						default:
							params[i] =t.getFromHeap((int) stack.pop());
							//if(params[i] instanceof ClassInstance)
								//params[i]=((ClassInstance)params[i]).getNativeInstance();
					}
					j++;
				}
				Object obj=  t.getFromHeap((int) stack.pop());
				if(obj instanceof ClassInstance)
					obj=((ClassInstance)obj).getNativeInstance();
				Object r=((java.lang.reflect.Method)m).invoke(obj, params);
				switch(returntype){
					case "V":
						return;
					default:
						stack.push(t.addToHeap(r));
					return;
				}
				
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}
	}
	@Override
	public byte getTag()
	{
		// TODO: Implement this method
		return 0;
	}

	@Override
	public void setTag(byte tag)
	{
		// TODO: Implement this method
	}

	@Override
	public short getId()
	{
		// TODO: Implement this method
		return 0;
	}

	@Override
	public void setId(short id)
	{
		// TODO: Implement this method
	}

	@Override
	public int getParamsCount()
	{
		// TODO: Implement this method
		return 0;
	}

	@Override
	public String getName()
	{
		// TODO: Implement this method
		return null;
	}

	@Override
	public String getNameAndDesc()
	{
		// TODO: Implement this method
		return null;
	}

	@Override
	public boolean isAbstract()
	{
		// TODO: Implement this method
		return false;
	}

	@Override
	public boolean isNative()
	{
		return true;
	}

	@Override
	public AClass getMyClass()
	{
		// TODO: Implement this method
		return null;
	}

	@Override
	public byte[] getCode()
	{
		// TODO: Implement this method
		return null;
	}

	@Override
	public void setNative(boolean b) {

	}

}
