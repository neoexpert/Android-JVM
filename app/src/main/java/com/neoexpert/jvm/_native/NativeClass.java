package com.neoexpert.jvm._native;
import com.neoexpert.jvm.*;
import com.neoexpert.jvm._abstract.*;
import com.neoexpert.jvm.constants.*;
import java.util.*;
import java.util.regex.*;
import java.lang.reflect.*;

public class NativeClass extends AClass
{

	@Override
	public Object getStatic(String str, String desc)
	{
		// TODO: Implement this method
		return null;
	}


	@Override
	public void putStatic(String fname, Object v)
	{
		// TODO: Implement this method
	}


	@Override
	public void putStatic(int id, Object obj)
	{
		// TODO: Implement this method
	}


	@Override
	public boolean isInitialized()
	{
		// TODO: Implement this method
		return false;
	}


	@Override
	public void clinit()
	{
		// TODO: Implement this method
	}


	@Override
	public Constant getConstant(int i)
	{
		// TODO: Implement this method
		return null;
	}


	@Override
	public boolean isInstance(Object obj)
	{
		return cl.isInstance(obj);
	}

	@Override
	public boolean isInstance(AClass obj) {
		return false;
	}

	private java.lang.Class cl;
	public NativeClass(java.lang.Class cl){
		this.cl=cl;
	}
	@Override
	public String getName()
	{
		// TODO: Implement this method
		return null;
	}


	@Override
	public AClass getClass(int id)
	{
		// TODO: Implement this method
		return null;
	}
	
	private ArrayList<String> descriptorToTypes(String descriptor) {
		descriptor= descriptor.substring(1);
		int pt=descriptor.indexOf(')');
		String returnType=descriptor.substring(pt+1);
		descriptor=descriptor.substring(0,pt);
       
        ArrayList<String> types=new ArrayList<>();
		types.add(returnType);
        
        while(!descriptor.isEmpty())
        {
			String type;
			char t=descriptor.charAt(0);
			if(t=='L')
			{
				int p=descriptor.indexOf(';');
				type = descriptor.substring(1,p);
				descriptor= descriptor.substring(p+1,descriptor.length());
			}
			else{
				type=t+"";
				descriptor=descriptor.substring(1);
			}
			types.add(type);
		} 

        return types;
    }
	@Override
	public AMethod getMethod(String mname,String parameters, int id)
	{
		ArrayList<String> types=descriptorToTypes(parameters);
		String returntype=types.remove(0); 
		try
		{
			
			java.lang.Class[] c=new java.lang.Class[types.size()];
			for (int i=0;i < types.size();i++)
			{
				switch (types.get(i))
				{
					case "I":
						c[i]=int.class;
						break;
					case "Z":
						c[i]=boolean.class;
						break;
					case "B":
						c[i]=byte.class;
						break;
					case "C":
						c[i]=char.class;
						break;
					case "D":
						c[i]=double.class;
						break;
					case "F":
						c[i]=float.class;
						break;
					case "J":
						c[i]=long.class;
						break;
					case "S":
						c[i]=short.class;
						break;
					default:
						c[i] = java.lang.Class.forName(types.get(i).replaceAll("/", "."));
				}
			}
			if(mname.equals("<init>"))
				return new NativeMethod(this,cl.getConstructor(c),types,null);
			else{
				
				return new NativeMethod(this,cl.getMethod(mname,c),types,returntype);
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}
		
	}


	@Override
	public Iterator<Constant> constantsIterator()
	{
		// TODO: Implement this method
		return null;
	}



	HashMap<String, Integer> staticFields=new HashMap<>();
	@Override
	public void getStatic(String name, String p1,Frame cF)
	{
		try
		{
			Integer addr=staticFields.get(name);
			
			java.lang.reflect.Field f=cl.getField(name);
			int m=f.getModifiers();
			if(Modifier.isStatic(m)){
				//here is the bug:
				//t should be current thread
				//addr=t.addToHeap(f.get(null));
				staticFields.put(name,addr);
				
			}
			throw new RuntimeException();
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}
	}

	@Override
	public AMethod getMethod(int i)
	{
		// TODO: Implement this method
		return null;
	}

	@Override
	public AMethod getMethod(String nameAndDesc) {
		return null;
	}

	@Override
	public void getStatic(int i,Frame heap)
	{
		// TODO: Implement this method
	}

	@Override
	public void getConstantInstance(int code,Frame cF)
	{
		// TODO: Implement this method
	}

	@Override
	public AMethod getMainMethod()
	{
		// TODO: Implement this method
		return null;
	}

	@Override
	public AMethod getCLInitMethod()
	{
		// TODO: Implement this method
		return null;
	}

	

	@Override
	public AMethod getMyMethod(String nameAndDesc)
	{
		// TODO: Implement this method
		return null;
	}

	@Override
	public int getFieldId(String field)
	{
		// TODO: Implement this method
		return 0;
	}

	@Override
	public HashMap<String, Object> getStaticFields()
	{
		// TODO: Implement this method
		return null;
	}

	@Override
	public AClass getSuperClass() {
		return null;
	}

	@Override
	public int getMyInstanceRef() {
		return 0;
	}


}
