package com.neoexpert.jvm;

import com.neoexpert.jvm._abstract.*;
import com.neoexpert.jvm.adapter.*;
import com.neoexpert.jvm.constants.*;
import com.neoexpert.jvm.method.*;
import com.neoexpert.jvm.natives.*;
import java.util.*;

public class MyThread {
    private static final HashMap<Integer, Object> heap = new HashMap<>();
    private static int addr = 1;

	public int this_thread;

	private Frame last_frame;

	public static void reset()
	{
		heap.clear();
	}

	public int getStackSize()
	{
		return stack.size();
	}

	public static void removeFromHeap(int addr)
	{
		heap.remove(addr);
	}

	

	

	public String getStackTrace()
	{
		Iterator<Frame> it=stack.iterator();
		StringBuilder sb=new StringBuilder();
		while (it.hasNext())
		{
			sb.append(it.next());
			sb.append("\n");
		}
		return sb.toString();
	}

	public AClass getCurrentClass()
	{
		return cC;
	}
	public Frame getPrevFrame()
	{
		Frame f=stack.pop();
		Frame pf=stack.peek();
		stack.push(f);
		return pf;
	}
	

    public static int addToHeap(Object c) {
        heap.put(addr, c);
        if (addr == 2703)
            heap.getClass();
        return addr++;
    }
	public static void addToHeap(int oref1, Object o)
	{
		heap.put(oref1,o);
	}

    public static Object getFromHeap(int a) {
        return heap.get(a);
    }

    private ClassAdapter classa;
    private OutputAdapter ma;

    public int newInstance(String method, int... args) {

        AMethod m = cC.getMyMethod(method);
        if (m == null) throw new RuntimeException("constructor " + method + " not found");
        Frame cF = new Frame(m);
        stack.push(cF);
        ClassInstance ci = new ClassInstance(cC);
        int oref = addToHeap(ci);

        cF.lv[0]=oref;
        for (int i = 0; i < args.length; i++) {
            cF.lv[i + 1]=args[i];
        }
        run();
        return (int) last_frame.lv[0];
    }

    public int newInstance2(AClass c, String method, int... args) {

        AMethod m = cC.getMyMethod(method);
        if (m == null) throw new RuntimeException("constructor " + method + " not found");
        Frame cF = new Frame(m);
        stack.push(cF);
        ClassInstance ci = new ClassInstance(c);
        int oref = addToHeap(ci);

        cF.lv[0]=oref;
        for (int i = 0; i < args.length; i++) {
            cF.lv[i + 1]= args[i];
        }
        //run();
        return oref;
    }

    public MyThread setMethodToRun(String method,int ... args) {
        AMethod m = cC.getMyMethod(method);
        if (m == null) return this;
        Frame cF = new Frame(m);
		for(int i=0;i<args.length;i++){
			cF.lv[i]=args[i];
		}
        stack.push(cF);
		if(classa!=null)
			classa.setFrame(cF);
        return this;
    }

    public int getCurrentLine() {
        if (stack.isEmpty())
            return -1;
        return stack.peek().getCurrentLine();
    }

    public void setClassAdapter(ClassAdapter classa) {
        this.classa = classa;
    }


    // current class
    AClass cC;
    // current Frame
    // Frame cF;

    // Frame stack
    private Stack<Frame> stack = new Stack<>();
	private Stack<Frame> exceptionstack = new Stack<>();
	public Stack<Frame> getExceptionStack(){
		return exceptionstack;
	}
    private StackAdapter sa;

    // private byte[] code;

    private CodeAdapter ca;

    private LvAdapter lva;

    private FrameStackAdapter fsa;


    public MyThread(AClass c) {


        this.cC = c;

        // AClass tc=com.neoexpert.jvm.Class.getClass("java/lang/Thread");

        //ClassInstance ic=new ClassInstance(tc);
        //this_thread=addToHeap(ic);
    }

    public MyThread(AClass c, String name, OutputAdapter ma) {
        this.ma = ma;


        this.cC = c;
        if (name != null && JVM.system_thread_group != 0)
            createThread(name);
        // AClass tc=com.neoexpert.jvm.Class.getClass("java/lang/Thread");

        //ClassInstance ic=new ClassInstance(tc);
        //this_thread=addToHeap(ic);
    }

    public void createThread(String name) {
        int tname = NString.createInstance(name);
        AClass thc = com.neoexpert.jvm.Class.getClass("java/lang/Thread");
        MyThread t = new MyThread(thc, null, null);

        this_thread = t.newInstance2(thc, "<init>(Ljava/lang/ThreadGroup;Ljava/lang/String;)V", new int[]{JVM.system_thread_group, tname});
        ((ClassInstance) t.getFromHeap(this_thread)).putField("priority", 1);
        t.this_thread=this_thread;
        t.run();

    }

    public void setFrameStackAdapter(FrameStackAdapter fsa) {
        this.fsa = fsa;
        fsa.setStack(stack);
    }

    public void setLvAdapter(LvAdapter lv) {
        this.lva = lv;
        if (!stack.isEmpty())
            lv.setLv(stack.peek());
    }

    public void setCodeAdapter(CodeAdapter ca) {
        this.ca = ca;
        if (!stack.isEmpty())
            ca.setCode(stack.peek());
    }

    public void setStackAdapter(StackAdapter sa) {
        this.sa = sa;
        if (!stack.isEmpty())
            sa.setFrame(stack.peek());
    }

    public void pushFrame(Frame prevFrame, AMethod m) {
        Frame f = prevFrame.newRefFrame(m);
        stack.push(f);
        cC = m.getMyClass();

		
        if (classa != null)
            classa.setFrame(f);
			
        if (fsa != null)
            fsa.notifyDataSetChanged();
        //cF.code = m.getCode();
        if (ca != null) {
            ca.setCode(f);

            ca.setPc(f.pc);
        }
        if (sa != null)
            sa.setFrame(f);
        if (lva != null)
            lva.setLv(f);
    }

    // int pc=0;
    public boolean debuggerStep() {
        if (!step())
            return false;
        if (ca != null) {
            ca.setPc(stack.peek().pc);
        }
        if (sa != null)
            sa.notifyDataSetChanged();
        if (lva != null)
            lva.notifyDataSetChanged();

        return true;
    }

    public void run() {
        running = true;
        step();
        //while (step())
        //    continue;
    }

    private boolean running = false;
    public static int s = 0;

    //The Holy Grail
    private boolean step() {
		if (stack.isEmpty())
            return false;
		//local variables are better for performance
		Frame cF=stack.peek();
		int pc=cF.pc;
        ClassInstance c;
        int i1, i2, i3, i4, aref, oref;
        long l1, l2;
        float f1, f2;
        double d1, d2;
        AClass cl;
        AMethod m;
        ArrayInstance array;
        Object obj;
        Constant fc;
        Constant fname;
        Constant ftype;

        byte[] code = cF.code;
		Object[] lv=cF.lv;
        //byte op = code[pc];
		
        //if (cF.isThrowing())
            //op = (byte) 0xBF;
		
        final boolean running = this.running;
        mainLoop:
		do {
            ///*
            StringBuilder sb = new StringBuilder();
            CodeAttribute.parseOp(pc, code, sb);
            System.out.println(sb);
            s++;
            //*/
            
            switch (code[pc]) {
                case (byte) 0x00:
                    //nop
                    pc += 1;
                    continue;
                case (byte) 0x01:
                    //aconst_null
                    cF.push(0);
                    pc += 1;
                    continue;
                case (byte) 0x02:
                    //iconst_m1
                    cF.push(-1);
                    pc += 1;
                    continue;
                case (byte) 0x03:
                    //iconst_0
                    cF.push(0);

                    pc += 1;
                    continue;
                case (byte) 0x04:
                    //iconst_1
                    cF.push(1);

                    pc += 1;
                    continue;

                case (byte) 0x05:
                    //iconst_2
                    cF.push(2);

                    pc += 1;
                    continue;
                case (byte) 0x06:
                    //iconst_3
                    cF.push(3);

                    pc += 1;
                    continue;
                case (byte) 0x07:
                    //iconst_4
                    cF.push(4);

                    pc += 1;
                    continue;
                case (byte) 0x08:
                    //iconst_5
                    cF.push(5);

                    pc += 1;
                    continue;
                case (byte) 0x09:
                    //lconst_0
                    cF.push(0);
                    cF.push(0);
                    pc += 1;
                    continue;
                case (byte) 0x0A:
                    //lconst_1
                    cF.push(1);
                    cF.push(0);
                    pc += 1;
                    continue;
                case (byte) 0x0B:
                    //fconst_0
                    cF.push(0f);
                    pc += 1;
                    continue;
                case (byte) 0x0C:
                    //fconst_1
                    cF.push(1f);
                    pc += 1;
                    continue;
                case (byte) 0x0D:
                    //fconst_2
                    cF.push(2f);
                    pc += 1;
                    continue;
                case (byte) 0x0E:
                    //dconst_0
                    l1 = Double.doubleToRawLongBits(0.0);
                    i1 = (int) (l1 >> 32);
                    i2 = (int) l1;
                    cF.push(i1);
                    cF.push(i2);

                    pc += 1;
                    continue;
                case (byte) 0x0F:
                    //dconst_1
                    l1 = Double.doubleToRawLongBits(1.0);
                    i1 = (int) (l1 >> 32);
                    i2 = (int) l1;
                    cF.push(i1);
                    cF.push(i2);
                    pc += 1;
                    continue;
                case (byte) 0x10:
                    //bipush

                    cF.push((int) code[pc + 1]);
                    pc += 2;
                    continue;
                case (byte) 0x11:
                    //sipush
                    cF.push((ii(code[pc + 1], code[pc + 2])));
                    pc += 3;
                    continue;
                case (byte) 0x12:
                    //ldc
                    cC.getConstantInstance((int) code[pc + 1] & 0xFF, cF);

                    pc += 2;
                    continue;
                case (byte) 0x13:
                    //ldc_w
					cC.getConstantInstance((int) ii(code[pc + 1], code[pc + 2]), cF);
                    pc += 3;
                    continue;

                case (byte) 0x14:
                    //ldc2_w
                    cC.getConstantInstance((int) ii(code[pc + 1], code[pc + 2]), cF);


                    pc += 3;
                    continue;

                case (byte) 0x15:
                    //iload
                    cF.push(lv[(int) code[pc + 1]]);
                    pc += 2;
                    continue;
                case (byte) 0x16:
                    //lload
                    cF.push(lv[(int) code[pc + 1] + 1]);
                    cF.push(lv[(int) code[pc + 1]]);
                    pc += 2;
                    continue;
                case (byte) 0x17:
                    //fload
                    cF.push(lv[(int) code[pc + 1]]);
                    pc += 2;
                    continue;
                case (byte) 0x18:
                    //dload
                    cF.push(lv[(int) code[pc + 1] + 1]);
                    cF.push(lv[(int) code[pc + 1]]);

                    pc += 2;
                    continue;
                case (byte) 0x19:
                    //aload
                    cF.push(lv[(int) code[pc + 1]]);
                    pc += 2;
                    continue;
                case (byte) 0x1A:
                    //iload_0
                    cF.push(lv[0]);

                    pc += 1;
                    continue;
                case (byte) 0x1B:
                    //iload_1
                    cF.push(lv[1]);

                    pc += 1;
                    continue;
                case (byte) 0x1C:
                    //iload_2
                    cF.push(lv[2]);

                    pc += 1;
                    continue;
                case (byte) 0x1D:
                    //iload_3";
                    cF.push(lv[3]);

                    pc += 1;
                    continue;
                case (byte) 0x1E:
                    //lload_0;
                    cF.push(lv[1]);
                    cF.push(lv[0]);
                    pc += 1;
                    continue;
                case (byte) 0x1F:
                    //lload_1
                    cF.push(lv[2]);
                    cF.push(lv[1]);
                    pc += 1;
                    continue;
                case (byte) 0x20:
                    //lload_2
                    cF.push(lv[3]);
                    cF.push(lv[2]);
                    pc += 1;
                    continue;
                case (byte) 0x21:
                    //lload_3
                    cF.push(lv[4]);
                    cF.push(lv[3]);
                    pc += 1;
                    continue;
                case (byte) 0x22:
                    //fload_0
                    cF.push(lv[0]);

                    pc += 1;
                    continue;
                case (byte) 0x23:
                    //fload_1
                    cF.push(lv[1]);

                    pc += 1;
                    continue;
                case (byte) 0x24:
                    //fload_2
                    cF.push(lv[2]);

                    pc += 1;
                    continue;
                case (byte) 0x25:
                    //fload_3;
                    cF.push(lv[3]);

                    pc += 1;
                    continue;
                case (byte) 0x26:
                    //dload_0;
                    cF.push(lv[1]);
                    cF.push(lv[0]);
                    pc += 1;
                    continue;
                case (byte) 0x27:
                    //dload_1
                    cF.push(lv[2]);
                    cF.push(lv[1]);
                    pc += 1;
                    continue;
                case (byte) 0x28:
                    //dload_2
                    cF.push(lv[3]);
                    cF.push(lv[2]);
                    pc += 1;
                    continue;
                case (byte) 0x29:
                    //dload_3
                    cF.push(lv[4]);
                    cF.push(lv[3]);
                    pc += 1;
                    continue;
                case (byte) 0x2A:
                    //aload_0
                    cF.push(lv[0]);
                    pc += 1;
                    continue;
                case (byte) 0x2B:
                    //aload_1
                    cF.push(lv[1]);
                    pc += 1;
                    continue;
                case (byte) 0x2C:
                    //aload_2
                    cF.push(lv[2]);
                    pc += 1;
                    continue;
                case (byte) 0x2D:
                    //aload_3
                    cF.push(lv[3]);
                    pc += 1;
                    continue;
                case (byte) 0x2E:
                    //iaload
                    i1 = (int) cF.pop();
                    aref = (int) cF.pop();
                    array = (ArrayInstance) heap.get(aref);
                    cF.push((array.a)[i1]);
                    pc += 1;
                    continue;

                case (byte) 0x2F:
                    //laload
                    i1 = (int) cF.pop();
                    aref = (int) cF.pop();
					array = (ArrayInstance) heap.get(aref);
					l1= (long) (array.a)[i1];
					cF.push(l2i1(l1));
					cF.push(l2i2(l1));
                    pc += 1;
                    continue;
                case (byte) 0x30:
                    //faload
                    i1 = (int) cF.pop();
                    aref = (int) cF.pop();
                    array = (ArrayInstance) heap.get(aref);
                    cF.push((array.a)[i1]);
					
                    pc += 1;
                    continue;
                case (byte) 0x31:
                    //daload
                    i1 = (int) cF.pop();
                    aref = (int) cF.pop();
					array = (ArrayInstance) heap.get(aref);
                    cF.push((array.a)[i1]);
					
                    cF.push(42);
                    cF.push(42);
					
                    pc += 1;
					throw new RuntimeException("double arrays not implemented");
                    //continue;
                case (byte) 0x32:
                    //aaload
                    i1 = (int) cF.pop();
                    aref = (int) cF.pop();
                    array = (ArrayInstance) getFromHeap(aref);
                    if (array.a[i1] == null)
                        cF.push(0);
                    else
                        cF.push(array.a[i1]);

                    pc += 1;
                    continue;
                case (byte) 0x33:
                    //baload
                    i1 = (int) cF.pop();
                    aref = (int) cF.pop();
					array = (ArrayInstance) heap.get(aref);
                    cF.push((array.a)[i1]);
                    pc += 1;
                    continue;
                case (byte) 0x34:
                    //caload
                    i1 = (int) cF.pop();
                    aref = (int) cF.pop();
                    array=(ArrayInstance)getFromHeap(aref);
                    cF.push((int)((Character)array.a[i1]).charValue());
                    pc += 1;
                    continue;
                case (byte) 0x35:
                    //saload
                    i1 = (int) cF.pop();
                    aref = (int) cF.pop();
                    array=(ArrayInstance)getFromHeap(aref);
                    cF.push(array.a[i1]);
                    pc += 1;
                    continue;
                case (byte) 0x36:
                    //istore
                    lv[(int) code[pc + 1]]= (int) (int) cF.pop();
                    
                    pc += 2;
                    continue;
                case (byte) 0x37:
                    //lstore
                    lv[(int) code[pc + 1]]=(int) cF.pop();
                    lv[(int) code[pc + 1] + 1]= (int) cF.pop();
                    
                    pc += 2;
                    continue;
                case (byte) 0x38:
                    //fstore
                    lv[(int) code[pc + 1]]=(int) cF.pop();
                    
                    pc += 2;
                    continue;
                case (byte) 0x39:
                    //dstore
                    lv[(int) code[pc + 1]]= (int) cF.pop();
                    lv[(int) code[pc + 1] + 1]= (int) cF.pop();
                    
                    pc += 2;
                    continue;
                case (byte) 0x3A:
                    //astore
                    lv[(int) code[pc + 1]]=cF.pop();
                    
                    pc += 2;
                    continue;

                case (byte) 0x3B:
                    //istore_0
                    lv[0]= (int) cF.pop();
                    

                    pc += 1;
                    continue;
                case (byte) 0x3C:
                    //istore_1
                    lv[1]=(int) cF.pop();
                    
                    pc += 1;
                    continue;
                case (byte) 0x3D:
                    //istore_2
                    lv[2]= (int) cF.pop();
                    
                    pc += 1;
                    continue;
                case (byte) 0x3E:
                    //istore_3
                    lv[3]= (int) cF.pop();
                    
                    pc += 1;
                    continue;

                case (byte) 0x3F:
                    //lstore_0
                    lv[0]=(int) cF.pop();
                    lv[1]=(int) cF.pop();
                    
                    pc += 1;
                    continue;
                case (byte) 0x40:
                    //lstore_1
                    lv[1]=(int) cF.pop();
                    lv[2]=(int) cF.pop();
                    
                    pc += 1;
                    continue;
                case (byte) 0x41:
                    //store_2
                    lv[2]= (int) cF.pop();
                   	lv[3]=(int) cF.pop();
                    
                    pc += 1;
                    continue;
                case (byte) 0x42:
                    //lstore_3
                    lv[3]=(int) cF.pop();
                    lv[4]=(int) cF.pop();
                    
                    pc += 1;
                    continue;

                case (byte) 0x43:
                    //fstore_0
                    lv[0]= (int) cF.pop();
                    
                    pc += 1;
                    continue;
                case (byte) 0x44:
                    //fstore_1
                    lv[1]= (int) cF.pop();
                    
                    pc += 1;
                    continue;
                case (byte) 0x45:
                    //fstore_2
                    lv[2]= (int) cF.pop();
                    
                    pc += 1;
                    continue;
                case (byte) 0x46:
                    //fstore_3"
                    lv[3]=(int) cF.pop();
                    
                    pc += 1;
                    continue;

                case (byte) 0x47:
                    //dstore_0
                    lv[0]=(int) cF.pop();
                    lv[1]=(int) cF.pop();
                    
                    pc += 1;
                    continue;
                case (byte) 0x48:
                    //dstore_1
                    lv[1]= (int) cF.pop();
                    lv[2]= (int) cF.pop();
                    
                    pc += 1;
                    continue;
                case (byte) 0x49:
                    //dstore_2
                    lv[2]= (int) cF.pop();
                    lv[3]=(int) cF.pop();
                    
                    pc += 1;
                    continue;
                case (byte) 0x4A:
                    //dstore_3
                    lv[3]=(int) cF.pop();
                    lv[4]=(int) cF.pop();
                    
                    pc += 1;
                    continue;

                case (byte) 0x4B:
                    //astore_0
                    lv[0]= (int) cF.pop();
                    
                    pc += 1;
                    continue;
                case (byte) 0x4C:
                    //astore_1
                    lv[1]= (int) cF.pop();
                    
                    pc += 1;
                    continue;
                case (byte) 0x4D:
                    //astore_2
                   	lv[2]= (int) cF.pop();
                    
                    pc += 1;
                    continue;
                case (byte) 0x4E:
                    //astore_3
                    lv[3]= (int) cF.pop();
                    
                    pc += 1;
                    continue;
                case (byte) 0x4F:
                    //iastore
                    i1 = (int) cF.pop();
                    i2 = (int) cF.pop();
                    aref = (int) cF.pop();
                    array = (ArrayInstance) heap.get(aref);

                    (array.a)[i2] = i1;
                    pc += 1;
                    continue;
                case (byte) 0x50:
                    //lastore
                    l1 = (int) cF.pop();
                    l2 = (int) cF.pop();
                    i1 = (int) cF.pop();
                    aref = (int) cF.pop();
                    array = (ArrayInstance) heap.get(aref);
                    pc += 1;
                    throw new RuntimeException("implement me!");
                    //continue;
                case (byte) 0x51:
                    //fastore
                    f1 = (float) cF.pop();
                    i1 = (int) cF.pop();
                    aref = (int) cF.pop();
                    array = (ArrayInstance) heap.get(aref);
                    array.a[i1] = f1;
                    pc += 1;
                    continue;
                case (byte) 0x52:
                    //dastore
                    aref = (int) cF.pop();
                    i1 = (int) cF.pop();
                    i2 = (int) cF.pop();
                    i3 = (int) cF.pop();
                    pc += 1;
                    throw new RuntimeException("implement me!");
                    //continue;
                case (byte) 0x53:
                    //aastore
                    i1 = (int) cF.pop();
                    i2 = (int) cF.pop();
                    aref = (int) cF.pop();
                    array = (ArrayInstance) heap.get(aref);

                    (array.a)[i2] = i1;
                    pc += 1;
                    continue;
                case (byte) 0x54:
					//bastore
                    i1 = (int) cF.pop();
                    i2 = (int) cF.pop();
                    aref = (int) cF.pop();
                    array = (ArrayInstance) heap.get(aref);

                    (array.a)[i2] = i1;
                    pc += 1;
                    //throw new RuntimeException("implement me!");
                    continue;
                case (byte) 0x55:
                    //castore
                    i1 = (int) cF.pop();
                    i2 = (int) cF.pop();
                    aref = (int) cF.pop();

                    array = (ArrayInstance) heap.get(aref);
                    (array.a)[i2] = (char)i1;
                    pc += 1;

                    continue;
                case (byte) 0x56:
                    //sastore
                    i1 = (int) cF.pop();
                    i2 = (int) cF.pop();
                    aref = (int) cF.pop();
                    array = (ArrayInstance) heap.get(aref);

                    (array.a)[i2] = i1;
                    pc += 1;
                    continue;

                case (byte) 0x57:
                    //pop
                    cF.pop();
                    pc += 1;
                    continue;
                case (byte) 0x58:
                    //pop2
                    cF.pop();
                    cF.pop();
                    pc += 1;
                    continue;
                case (byte) 0x59:
                    //dup
                    cF.push(cF.peek());
                    pc += 1;
                    continue;

                case (byte) 0x5A:
                    //dup_x1
                    i1 = (int) cF.pop();
                    i2 = (int) cF.pop();

                    cF.push(i1);
                    cF.push(i2);
                    cF.push(i1);


                    pc += 1;
                    continue;

                case (byte) 0x5B:
                    //dup_x2
                    cF.push(cF.get(0));
                    pc += 1;
                    continue;
                case (byte) 0x5C:
                    //dup2
                    i2 = (int) cF.pop();
                    i1 = (int) cF.pop();
                    cF.push(i1);
                    cF.push(i2);
                    cF.push(i1);
                    cF.push(i2);

                    pc += 1;
                    continue;

                case (byte) 0x5D:
                    //dup2_x1
                    i2 = (int) cF.pop();
                    i1 = (int) cF.pop();
                    i3 = (int) cF.pop();


                    cF.push(i1);
                    cF.push(i2);

                    cF.push(i3);

                    cF.push(i1);
                    cF.push(i2);

                    pc += 1;
                    throw new RuntimeException("check that and contniue");

                    //continue;

                case (byte) 0x5E:
                    //dup2_x2
                    i2 = (int) cF.pop();
                    i1 = (int) cF.pop();
                    i4 = (int) cF.pop();
                    i3 = (int) cF.pop();

                    cF.push(i1);
                    cF.push(i2);

                    cF.push(i3);
                    cF.push(i4);

                    cF.push(i1);
                    cF.push(i2);
                    pc += 1;
                    throw new RuntimeException("check that and continue");

                    //continue;

                case (byte) 0x5F:
                    //swap
                    cF.push(cF.get(0));
                    pc += 1;
                    continue;

                case (byte) 0x60:
                    //iadd
                    i1 = (int) cF.pop();
                    i2 = (int) cF.pop();
                    cF.push(i1 + i2);

                    pc += 1;
                    continue;

                case (byte) 0x61:
                    //ladd

                    l1 = _2i2l((int) cF.pop(), (int) cF.pop());
                    l2 = _2i2l((int) cF.pop(), (int) cF.pop());
                    l1 = l2 + l1;
                    cF.push(l2i1(l1));
                    cF.push(l2i2(l1));

                    pc += 1;
                    continue;
                case (byte) 0x62:
                    //fadd
                    f1 = (float) cF.pop();
                    f2 = (float) cF.pop();
                    cF.push(f1 + f2);

                    pc += 1;

                    continue;
                case (byte) 0x63:
                    //dadd
                    l1 = _2i2l((int) cF.pop(), (int) cF.pop());
                    l2 = _2i2l((int) cF.pop(), (int) cF.pop());
   
                    d1 = Double.longBitsToDouble(l1);
                    d2 = Double.longBitsToDouble(l2);
                    d1 = d2 + d1;

                    l1 = Double.doubleToRawLongBits(d1);
                    i1 = (int) (l1 >> 32);
                    i2 = (int) l1;
                    cF.push(i1);
                    cF.push(i2);

                    pc += 1;

                    continue;

                case (byte) 0x64:
                    //isub
                    i1 = (int) cF.pop();
                    i2 = (int) cF.pop();
                    cF.push(i2 - i1);
                    pc += 1;
                    continue;
                case (byte) 0x65:
                    //lsub
                    l1 = _2i2l((int) cF.pop(), (int) cF.pop());
                    l2 = _2i2l((int) cF.pop(), (int) cF.pop());
                    l1 = l2 - l1;
                    cF.push(l2i1(l1));
                    cF.push(l2i2(l1));
                    pc += 1;
                    continue;
                case (byte) 0x66:
                    //fsub
                    f1 = (float) cF.pop();
                    f2 = (float) cF.pop();
                    cF.push(f2 - f1);
                    pc += 1;
                    continue;
                case (byte) 0x67:
                    //dsub
                    l1 = _2i2l((int) cF.pop(), (int) cF.pop());
                    l2 = _2i2l((int) cF.pop(), (int) cF.pop());
              
                    d1 = Double.longBitsToDouble(l1);
                    d2 = Double.longBitsToDouble(l2);
                    d1 = d2 - d1;

                    l1 = Double.doubleToRawLongBits(d1);
                    i1 = (int) (l1 >> 32);
                    i2 = (int) l1;
                    cF.push(i1);
                    cF.push(i2);
                    pc += 1;
                    continue;
                case (byte) 0x68:
                    //imul

                    i1 = (int) cF.pop();
                    i2 = (int) cF.pop();
                    cF.push(i2 * i1);
                    pc += 1;
                    continue;
                case (byte) 0x69:
                    //lmul
                    l1 = _2i2l((int) cF.pop(), (int) cF.pop());
                    l2 = _2i2l((int) cF.pop(), (int) cF.pop());
                    l1 = l1 * l2;
                    cF.push(l2i1(l1));
                    cF.push(l2i2(l1));
                    pc += 1;
                    continue;

                case (byte) 0x6A:
                    //fmul
                    f1 = (float) cF.pop();
                    f2 = (float) cF.pop();
                    cF.push(f2 * f1);
                    pc += 1;
                    continue;
                case (byte) 0x6B:
                    //dmul
                    l1 = _2i2l((int) cF.pop(), (int) cF.pop());
                    l2 = _2i2l((int) cF.pop(), (int) cF.pop());

                    d1 = Double.longBitsToDouble(l1);
                    d2 = Double.longBitsToDouble(l2);
                    d1 = d2 * d1;

                    l1 = Double.doubleToRawLongBits(d1);
                    cF.push(l2i1(l1));
                    cF.push(l2i2(l1));

                    pc += 1;
                    continue;
                case (byte) 0x6C:
                    //idiv
                    i1 = (int) cF.pop();
                    i2 = (int) cF.pop();
                    cF.push(i2 / i1);

                    pc += 1;
                    continue;
                case (byte) 0x6D:
                    //ldiv
                    l1 = _2i2l((int) cF.pop(), (int) cF.pop());
                    l2 = _2i2l((int) cF.pop(), (int) cF.pop());
                    l1 = l2 / l1;
                    cF.push(l2i1(l1));
                    cF.push(l2i2(l1));

                    pc += 1;
                    continue;
                case (byte) 0x6E:
                    //fdiv
                    f1 = (float) cF.pop();
                    f2 = (float) cF.pop();
                    cF.push(f2 / f1);

                    pc += 1;
                    continue;
                case (byte) 0x6F:
                    //ddiv
                    l1 = _2i2l((int) cF.pop(), (int) cF.pop());
                    l2 = _2i2l((int) cF.pop(), (int) cF.pop());

                    d1 = Double.longBitsToDouble(l1);
                    d2 = Double.longBitsToDouble(l2);
                    d1 = d2 / d1;

                    l1 = Double.doubleToRawLongBits(d1);
                    cF.push(l2i1(l1));
                    cF.push(l2i2(l1));
                    pc += 1;
                    continue;
                case (byte) 0x70:
                    //irem
                    i1 = (int) cF.pop();
                    i2 = (int) cF.pop();
                    cF.push(i2 % i1);
                    pc += 1;
                    continue;

                case (byte) 0x71:
                    //lrem
                    l1 = _2i2l((int) cF.pop(), (int) cF.pop());
                    l2 = _2i2l((int) cF.pop(), (int) cF.pop());
                    l1 = l2 % l1;
                    cF.push(l2i1(l1));
                    cF.push(l2i2(l1));
                    pc += 1;
                    continue;

                case (byte) 0x72:
                    //frem
                    f1 = (int) cF.pop();
                    f2 = (int) cF.pop();
                    cF.push(f2 % f1);
                    pc += 1;
                    continue;

                case (byte) 0x73:
                    //drem
                    l1 = _2i2l((int) cF.pop(), (int) cF.pop());
                    l2 = _2i2l((int) cF.pop(), (int) cF.pop());

                    d1 = Double.longBitsToDouble(l1);
                    d2 = Double.longBitsToDouble(l2);
                    d1 = d2 % d1;

                    l1 = Double.doubleToRawLongBits(d1);
                    i1 = (int) (l1 >> 32);
                    i2 = (int) l1;
                    cF.push(i1);
                    cF.push(i2);
                    pc += 1;
                    continue;

                case (byte) 0x74:
                    //ineg
                    cF.push(-(int) cF.pop());
                    pc += 1;
                    continue;
                case (byte) 0x75:
                    //lneg
                    l1 = _2i2l((int) cF.pop(), (int) cF.pop());

                    l1 = -l1;
                    cF.push(l2i1(l1));
                    cF.push(l2i2(l1));

                    pc += 1;
                    continue;
                case (byte) 0x76:
                    //fneg
                    cF.push(-(float) cF.pop());
                    pc += 1;
                    continue;
                case (byte) 0x77:
                    //dneg

                    l1 = _2i2l((int) cF.pop(), (int) cF.pop());

                    d1 = Double.longBitsToDouble(l1);

                    d1 = -d1;

                    l1 = Double.doubleToRawLongBits(d1);
                    i1 = (int) (l1 >> 32);
                    i2 = (int) l1;
                    cF.push(i1);
                    cF.push(i2);
                    pc += 1;

                    continue;
                case (byte) 0x78:
                    //ishl
                    i1 = (int) cF.pop();
                    i2 = (int) cF.pop();
                    cF.push(i2 << i1);
                    pc += 1;
                    continue;
                case (byte) 0x79:
                    //lshl
                    i1 = (int) cF.pop();
                    i2 = (int) cF.pop();
                    i3 = (int) cF.pop();
                    l1 = _2i2l(i2, i3) << i1;

                    cF.push(l2i1(l1));
                    cF.push(l2i2(l1));
                    pc += 1;
                    continue;
                case (byte) 0x7A:
                    //ishr
                    i1 = (int) cF.pop();
                    i2 = (int) cF.pop();
                    cF.push(i2 >> i1);
                    pc += 1;
                    continue;
                case (byte) 0x7B:
                    //lshr
                    i1 = (int) cF.pop();

                    l2 = _2i2l((int) cF.pop(), (int) cF.pop());
                    l1 = l2 >> i1;
                    cF.push(l2i1(l1));
                    cF.push(l2i2(l1));
                    pc += 1;

                    continue;

                case (byte) 0x7C:
                    //iushr
                    i1 = (int) cF.pop();
                    i2 = (int) cF.pop();
                    cF.push(i2 >>> i1);
                    pc += 1;

                    continue;
                case (byte) 0x7D:
                    //iushr
                    i1 = (int) cF.pop();
                    l2 = (long) cF.pop();
                    cF.push(l2 >>> i1);
                    pc += 1;
                    continue;
                case (byte) 0x7E:
                    //iand
                    i1 = (int) cF.pop();
                    i2 = (int) cF.pop();
                    cF.push(i2 & i1);
                    pc += 1;
                    continue;
                case (byte) 0x7F:
                    //land
                    l1 = _2i2l((int) cF.pop(), (int) cF.pop());
                    l2 = _2i2l((int) cF.pop(), (int) cF.pop());
                    l1 = l2 & l1;

                    cF.push(l2i1(l1));
                    cF.push(l2i2(l1));
                    pc += 1;
                    continue;
                case (byte) 0x80:
                    //ior
                    i1 = (int) cF.pop();
                    i2 = (int) cF.pop();
                    cF.push(i2 | i1);
                    pc += 1;
                    continue;
                case (byte) 0x81:
                    //lor
                    l1 = _2i2l((int) cF.pop(), (int) cF.pop());
                    l2 = _2i2l((int) cF.pop(), (int) cF.pop());
                    l1 = l2 | l1;

                    cF.push(l2i1(l1));
                    cF.push(l2i2(l1));
                    pc += 1;
                    continue;
                case (byte) 0x82:
                    //ixor
                    i1 = (int) cF.pop();
                    i2 = (int) cF.pop();
                    cF.push(i2 ^ i1);
                    pc += 1;
                    continue;
                case (byte) 0x83:
                    //lxor
                    l1 = _2i2l((int) cF.pop(), (int) cF.pop());
                    l2 = _2i2l((int) cF.pop(), (int) cF.pop());
                    l1 = l2 ^ l1;

                    cF.push(l2i1(l1));
                    cF.push(l2i2(l1));
                    pc += 1;
                    continue;
                case (byte) 0x84:
                    //iinc
                    i1 = (int) lv[(int) code[pc + 1]];
                    lv[(int) code[pc + 1]]= i1 + code[pc + 2];
                    pc += 3;
                    continue;
                case (byte) 0x85:
                    //i2l
                    cF.push(cF.pop());
                    cF.push(0);
                    pc += 1;
                    continue;
                case (byte) 0x86:
                    //i2f
                    cF.push(Float.valueOf((int) cF.pop()));
                    pc += 1;
                    continue;
                case (byte) 0x87:
                    //i2d
                    i1 = (int) cF.pop();
                    l1 = Double.doubleToRawLongBits(i1);
                    i1 = (int) (l1 >> 32);
                    i2 = (int) l1;
                    cF.push(i1);
                    cF.push(i2);

                    pc += 1;
                    continue;
                case (byte) 0x88:
                    //l2i
                    l1 = _2i2l((int) cF.pop(), (int) cF.pop());
                    cF.push((int) l1);
                    pc += 1;
                    continue;
                case (byte) 0x89:
                    //l2f
                    l1 = _2i2l((int) cF.pop(), (int) cF.pop());
                    cF.push((float) l1);
                    pc += 1;
                    continue;
                case (byte) 0x8A:
                    //l2d
                    l1 = _2i2l((int) cF.pop(), (int) cF.pop());

                    d1 = (double) l1;

                    l1 = Double.doubleToRawLongBits(d1);
                    i1 = (int) (l1 >> 32);
                    i2 = (int) l1;
                    cF.push(i1);
                    cF.push(i2);
                    pc += 1;
                    continue;
                case (byte) 0x8B:
                    //f2i
                    cF.push(((Float) cF.pop()).intValue());
                    pc += 1;
                    continue;
                case (byte) 0x8C:
                    //f2l
                    f1 = (float) cF.pop();

                    l1 = (long) f1;
                    cF.push(l2i1(l1));
                    cF.push(l2i2(l1));

                    pc += 1;
                    continue;
                case (byte) 0x8D:
                    //f2d

                    f1 = (float) cF.pop();

                    d1 = (double) f1;

                    l1 = Double.doubleToRawLongBits(d1);
                    i1 = (int) (l1 >> 32);
                    i2 = (int) l1;
                    cF.push(i1);
                    cF.push(i2);
                    pc += 1;
                    continue;
                case (byte) 0x8E:
                    //d2i
                    l1 = _2i2l((int) cF.pop(), (int) cF.pop());

                    d1 = Double.longBitsToDouble(l1);

                    cF.push((int) d1);
                    pc += 1;
                    continue;
                case (byte) 0x8F:
                    //d2l
                    l1 = _2i2l((int) cF.pop(), (int) cF.pop());

                    d1 = Double.longBitsToDouble(l1);
                    l1 = (long) d1;
                    cF.push(l2i1(l1));
                    cF.push(l2i2(l1));

                    pc += 1;
                    continue;
                case (byte) 0x90:
                    //d2f
                    l1 = _2i2l((int) cF.pop(), (int) cF.pop());

                    d1 = Double.longBitsToDouble(l1);

                    cF.push((float) d1);
                    pc += 1;
                    continue;
                case (byte) 0x91:
                    //i2b
                    cF.push((int)((Integer)cF.pop()).byteValue());
                    pc += 1;
                    continue;
                case (byte) 0x92:
                    //i2c
                    cF.push(((Integer) cF.pop()).intValue());
                    pc += 1;
                    continue;
                case (byte) 0x93:
                    //i2s
                    cF.push((short) cF.pop());
                    pc += 1;
                    continue;
                case (byte) 0x94:
                    //lcmp
                    l1 = _2i2l((int) cF.pop(), (int) cF.pop());
                    l2 = _2i2l((int) cF.pop(), (int) cF.pop());

                    if (l2 == l1) {
                        cF.push(0);
                    } else if (l2 > l1) {
                        cF.push(1);
                    } else
                        cF.push(-1);
                    pc += 1;
                    continue;
                case (byte) 0x95:
                    //fcmpl
                    f1 = (float) cF.pop();
                    f2 = (float) cF.pop();
                    if (f2 == f1) {
                        cF.push(0);
                    } else if (f2 > f1) {
                        cF.push(1);
                    } else if (f2 < f1)
                        cF.push(-1);
                    else
                        cF.push(-1);
                    pc += 1;
                    continue;
                case (byte) 0x96:
                    //fcmpg
                    f1 = (float) cF.pop();
                    f2 = (float) cF.pop();
                    if (f2 == f1) {
                        cF.push(0);
                    } else if (f2 > f1) {
                        cF.push(1);
                    } else if (f2 < f1)
                        cF.push(-1);
                    else
                        cF.push(1);

                    pc += 1;
                    continue;
                case (byte) 0x97:
                    //dcmpl
                    l1 = _2i2l((int) cF.pop(), (int) cF.pop());
                    l2 = _2i2l((int) cF.pop(), (int) cF.pop());

                    d1 = Double.longBitsToDouble(l1);
                    d2 = Double.longBitsToDouble(l2);


                    if (d2 == d1) {
                        cF.push(0);
                    } else if (d2 > d1) {
                        cF.push(1);
                    } else if (d2 < d1)
                        cF.push(-1);
                    else
                        cF.push(-1);
                    pc += 1;
                    continue;
                case (byte) 0x98:
                    //dcmpg
                    l1 = _2i2l((int) cF.pop(), (int) cF.pop());
                    l2 = _2i2l((int) cF.pop(), (int) cF.pop());
                   
                    d1 = Double.longBitsToDouble(l1);
                    d2 = Double.longBitsToDouble(l2);
                    if (d2 == d1) {
                        cF.push(0);
                    } else if (d2 > d1) {
                        cF.push(1);
                    } else if (d2 < d1)
                        cF.push(-1);
                    else
                        cF.push(1);
                    pc += 1;
                    continue;
                case (byte) 0x99:
                    //ifeq
                    if ((int) cF.pop() == 0)
                        pc += i(code[pc + 1], code[pc + 2]);
                    else
                        pc += 3;
                    continue;

                case (byte) 0x9A:
                    //ifne
                    if ((int) cF.pop() != 0)
                        pc += i(code[pc + 1], code[pc + 2]);
                    else
                        pc += 3;
                    continue;
                case (byte) 0x9B:
                    //iflt
                    if ((int) cF.pop() < 0)
                        pc += i(code[pc + 1], code[pc + 2]);
                    else
                        pc += 3;
                    continue;

                case (byte) 0x9C:
                    //ifge
                    if ((int) cF.pop() >= 0)
                        pc += i(code[pc + 1], code[pc + 2]);
                    else
                        pc += 3;
                    continue;
                case (byte) 0x9D:
                    //ifgt
                    if ((int) cF.pop() > 0)
                        pc += i(code[pc + 1], code[pc + 2]);
                    else
                        pc += 3;
                    continue;
                case (byte) 0x9E:
                    //ifle
                    if ((int) cF.pop() <= 0)
                        pc += i(code[pc + 1], code[pc + 2]);
                    else
                        pc += 3;
                    continue;
                case (byte) 0x9F:
                    //if_icmpeq
                    if ((int) cF.pop() == (int) cF.pop())
                        pc += i(code[pc + 1], code[pc + 2]);
                    else
                        pc += 3;
                    continue;
                case (byte) 0xA0:
                    //if_icmpne
                    if ((int) cF.pop() != (int) cF.pop())
                        pc += i(code[pc + 1], code[pc + 2]);
                    else
                        pc += 3;
                    continue;
                case (byte) 0xA1:

                    //if_icmplt
                    if ((int) cF.pop() > (int) cF.pop())
                        pc += i(code[pc + 1], code[pc + 2]);
                    else
                        pc += 3;
                    continue;

                case (byte) 0xA2:
                    //if_icmpge
                    if ((int) cF.pop() <= (int) cF.pop())
                        pc += i(code[pc + 1], code[pc + 2]);
                    else
                        pc += 3;
                    continue;

                case (byte) 0xA3:
                    //if_icmpgt
                    if ((int) cF.pop() < (int) cF.pop())
                        pc += i(code[pc + 1], code[pc + 2]);
                    else
                        pc += 3;
                    continue;

                case (byte) 0xA4:
                    //if_icmple
                    if ((int) cF.pop() >= (int) cF.pop())
                        pc += i(code[pc + 1], code[pc + 2]);
                    else
                        pc += 3;
                    continue;
                case (byte) 0xA5:
                    //if_acmpeq
                    if ((int) cF.pop() == (int) cF.pop())
                        pc += i(code[pc + 1], code[pc + 2]);
                    else
                        pc += 3;
                    continue;
                case (byte) 0xA6:
					//if_acmpeq
                    if ((int) cF.pop() != (int) cF.pop())
                        pc += i(code[pc + 1], code[pc + 2]);
                    else
                        pc += 3;
                    continue;
                case (byte) 0xA7:
                    //goto


                    pc+= i(code[pc + 1], code[pc + 2]);
                    
                    continue;
				case (byte)0xA8:
					//jsr
					cF.push(pc+3);
					pc+= i(code[pc + 1], code[pc + 2]);
					continue;
				case (byte)0xA9:
					//ret
					pc= (int) lv[code[pc+1]];
					continue;
                case (byte) 0xAA:
                    //tableswitch
                    i1 = (int) cF.pop();
                    int pc2 = pc + (4 - pc % 4);

                    int default_offset = i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);

                    pc2 += 4;
                    int low = i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
                    pc2 += 4;
                    int high = i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
                    pc2 += 4;
                    if (i1 < low || i1 > high) {  // if its less than <low> or greater than <high>,
                        pc += default_offset;
                        // branch to default
                    } else {
                        pc2 += (i1 - low) * 4;
                        pc += i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
                    }
                    continue;
                case (byte) 0xAB:
                    //lookupswitch
                    i1 = (int) cF.pop();
                    pc2 = pc + (4 - pc % 4);

                    default_offset = i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
                    pc2 += 4;
                    i2 = i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
                    pc2 += 4;
                    for (i3 = 0; i3 < i2; i3++) {
                        int key = i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);

                        if (key == i1) {
                            pc2 += 4;
                            int value = i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
                            pc += value;
                            continue mainLoop;
                        }
                        pc2 += 8;
                    }
                    pc += default_offset;
                    continue;
                case (byte) 0xAC:
                    //ireturn
                    i1 = (int) cF.pop();
                    pc += 1;

                    last_frame = stack.pop();
                    if (stack.isEmpty()) {
                        if (ca != null)
                            ca.setCode(null);
                        if (sa != null)
                            sa.setFrame(null);
                        if (lva != null)
                            lva.setLv(null);
                        return false;
                    }
                    cF = stack.peek();
                    pc = cF.pc;
                    code = cF.code;
					lv=cF.lv;
                    cC = cF.getMyClass();
                    System.out.println("ireturn: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getNameAndDesc());
                    if (classa != null)
                        classa.setFrame(cF);

                    if (fsa != null)
                        fsa.notifyDataSetChanged();
                    cF.push(i1);
                    // this.code=c.getMethod(11).attrs[0].code;
                    if (ca != null) {
                        ca.setCode(cF);
                        ca.setPc(pc);
                    }
                    if (sa != null)
                        sa.setFrame(cF);
                    if (lva != null)
                        lva.setLv(cF);
                    continue;
                case (byte) 0xAD:
                    //lreturn
                    i2 = (int) cF.pop();
                    i1 = (int) cF.pop();

                    pc += 1;

                    last_frame = stack.pop();
                    if (stack.isEmpty()) {
                        if (ca != null)
                            ca.setCode(null);
                        if (sa != null)
                            sa.setFrame(null);
                        if (this.lva != null)
                            this.lva.setLv(null);
                        return false;
                    }
                    cF = stack.peek();
                    pc = cF.pc;
                    code = cF.code;
					lv=cF.lv;
                    cC = cF.getMyClass();
                    if (classa != null)
                        classa.setFrame(cF);

                    if (fsa != null)
                        fsa.notifyDataSetChanged();
                    cF.push(i1);
                    cF.push(i2);

                    // this.code=c.getMethod(11).attrs[0].code;
                    System.out.println("lreturn: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getNameAndDesc());
                    if (ca != null) {
                        ca.setCode(cF);
                        ca.setPc(pc);
                    }
                    if (sa != null)
                        sa.setFrame(cF);
                    if (lva != null)
                        lva.setLv(cF);
                    continue;
                case (byte) 0xAE:
                    //freturn
                    obj = cF.pop();
                    pc += 1;

                    last_frame = stack.pop();
                    if (stack.isEmpty()) {
                        if (ca != null)
                            ca.setCode(null);
                        if (sa != null)
                            sa.setFrame(null);
                        if (lva != null)
                            lva.setLv(null);
                        return false;
                    }
                    cF = stack.peek();
                    pc = cF.pc;
                    code = cF.code;
					lv=cF.lv;
                    cC = cF.getMyClass();
                    System.out.println("freturn: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getNameAndDesc());
                    if (classa != null)
                        classa.setFrame(cF);

                    if (fsa != null)
                        fsa.notifyDataSetChanged();
                    cF.push(obj);
                    // this.code=c.getMethod(11).attrs[0].code;
                    if (ca != null) {
                        ca.setCode(cF);
                        ca.setPc(pc);
                    }
                    if (sa != null)
                        sa.setFrame(cF);
                    if (lva != null)
                        lva.setLv(cF);

                    continue;
                case (byte) 0xAF:
                    //dreturn
                    i2 = (int) cF.pop();
                    i1 = (int) cF.pop();
                    pc += 1;

                    last_frame = stack.pop();
                    if (stack.isEmpty()) {
                        if (ca != null)
                            ca.setCode(null);
                        if (sa != null)
                            sa.setFrame(null);
                        if (lva != null)
                            lva.setLv(null);
                        return false;
                    }
                    cF = stack.peek();
                    pc = cF.pc;
                    code = cF.code;
					lv=cF.lv;
                    cC = cF.getMyClass();
                    System.out.println("dreturn: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getNameAndDesc());
                    if (classa != null)
                        classa.setFrame(cF);

                    if (fsa != null)
                        fsa.notifyDataSetChanged();
                    cF.push(i1);
                    cF.push(i2);

                    // this.code=c.getMethod(11).attrs[0].code;
                    if (ca != null) {
                        ca.setCode(cF);
                        ca.setPc(pc);
                    }
                    if (sa != null)
                        sa.setFrame(cF);
                    if (lva != null)
                        lva.setLv(cF);

                    continue;
                  case (byte) 0xB0:
                    //areturn
                    i1 = (int) cF.pop();
                    pc += 1;

                    last_frame = stack.pop();
                    if (stack.isEmpty()) {
                        if (ca != null)
                            ca.setCode(null);
                        if (sa != null)
                            sa.setFrame(null);
                        if (lva != null)
                            lva.setLv(null);
                        return false;
                    }
                    cF = stack.peek();
                    pc = cF.pc;
                    code = cF.code;
					lv=cF.lv;
                    cC = cF.getMyClass();
                      System.out.println("areturn: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getNameAndDesc());
                    if (classa != null)
                        classa.setFrame(cF);

                    if (fsa != null)
                        fsa.notifyDataSetChanged();
                    cF.push(i1);
                    // this.code=c.getMethod(11).attrs[0].code;
                    if (ca != null) {
                        ca.setCode(cF);
                        ca.setPc(pc);
                    }
                    if (sa != null)
                        sa.setFrame(cF);
                    if (lva != null)
                        lva.setLv(cF);
                    continue;
				case (byte) 0xB1:
                    //return
                    pc += 1;
                    last_frame = stack.pop();
                    if (stack.isEmpty()) {
                        if (ca != null)
                            ca.setCode(null);
                        if (sa != null)
                            sa.setFrame(null);
                        if (lva != null)
                            lva.setLv(null);
                        return false;
                    }
                    cF = stack.peek();
                    pc = cF.pc;
                    code = cF.code;
					lv=cF.lv;
                    cC = cF.getMyClass();
                    System.out.println("return: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getNameAndDesc());
                    if (classa != null)
                        classa.setFrame(cF);
                    if (fsa != null)
                        fsa.notifyDataSetChanged();

                    // this.code=c.getMethod(11).attrs[0].code;
                    if (ca != null) {
                        ca.setCode(cF);
                        ca.setPc(pc);
                    }
                    if (sa != null)
                        sa.setFrame(cF);
                    if (lva != null)
                        lva.setLv(cF);
                    continue;
					
                case (byte) 0xB2:
                    //getstatic

                    // v=cC.staticfields.get(((code[i + 1] << 8) | code[i + 2]));

                    cC.getStatic(ii(code[pc + 1], code[pc + 2]), cF);
                    // cF.push(o);
                    pc += 3;
                    continue;

                case (byte) 0xB3:
                    //putstatic
                    obj = cF.pop();
                    fc = cC.getConstant(ii(code[pc + 1], code[pc + 2]));
                    fc = cC.getConstant(fc.index2);
                    fname = cC.getConstant(fc.index1);
                    ftype = cC.getConstant(fc.index2);

                    switch (ftype.str) {
                        case "J":
                            obj = ((Integer) obj).longValue() << 32 | ((Integer) (cF.pop())).longValue() & 0xFFFFFFFFL;
                            break;
                        case "D":
                            l1 = ((Integer) obj).longValue() << 32 | ((Integer) (cF.pop())).longValue() & 0xFFFFFFFFL;
                            obj = Double.longBitsToDouble(l1);

                            //throw new RuntimeException("putfield error");
                            break;
                    }

                    // int addrr=(code[i+1] << 8) | code[i+2];
					cC.putStatic(fname.str,obj);
                    
                    pc += 3;
                    continue;
                case (byte) 0xB4:
                    //getfield
                    oref = (int) cF.pop();
                    obj = (Instance) heap.get(oref);
                    fc = cC.getConstant(ii(code[pc + 1], code[pc + 2]));
                    fc = cC.getConstant(fc.index2);
                    fname = cC.getConstant(fc.index1);
                    ftype = cC.getConstant(fc.index2);
                    if (obj != null)
                        obj = ((ClassInstance) obj).getField(fname.str);
                    else obj = 0;
                    switch (ftype.str) {
                        case "J":
                            cF.push(l2i1((Long) obj));
                            cF.push(l2i2((Long) obj));
                            break;
                        case "D":
                            throw new RuntimeException("getfield error");
                            //break;
                        default:
                            cF.push(obj);
                            break;
                    }


                    pc += 3;
                    continue;
                case (byte) 0xB5:
                    //putfield
                    obj = cF.pop();
                    fc = cC.getConstant(ii(code[pc + 1], code[pc + 2]));
                    fc = cC.getConstant(fc.index2);
                    fname = cC.getConstant(fc.index1);
                    ftype = cC.getConstant(fc.index2);

                    switch (ftype.str) {
                        case "J":
                            obj = ((Integer) obj).longValue() << 32 | ((Integer) (cF.pop())).longValue() & 0xFFFFFFFFL;
                            break;
                        case "D":
                            throw new RuntimeException("putfield error");
                            //break;
                    }
                    Object o = cF.pop();
                    try {
                        oref = (int) o;
                    }
                    catch (NullPointerException e){
                       getClass();
                       oref=0;
                    }
                    c = (ClassInstance) heap.get(oref);
                    if (c == null)
                        throw new NullPointerException("field " + fname.str + " reference: " + oref + " this_thread: " + this_thread);

                    c.putField(fname.str, obj);
                    pc += 3;
                    continue;

                case (byte) 0xB6:
                    //invokevirtual
                    // int objref=(int)cF.pop();
                    if (fsa != null)
                        fsa.notifyDataSetChanged();
                    m = cC.getMethod(ii(code[pc + 1], code[pc + 2]));
                    pc += 3;
                    cF.pc = pc;
                    System.out.println("invokevirtual: " + m.getMyClass().getName() + "." + m.getNameAndDesc());
                    //Frame nf = cF.newRefFrame(m);


                    Frame nf = cF.newRefFrame(m);
                    if (m.isAbstract()) {
                        oref = (int) nf.lv[0];
                        m = ((Instance) heap.get(oref)).getMyClass().getMethod(m.getNameAndDesc());
                        nf.initLocals(m);
						nf.setMethod(m);
                    }
                    if (m.isNative()) {
                        invokeNative(m.getMyClass().getName(), m.getNameAndDesc(),cF, nf);
                        cF = stack.peek();
                        pc = cF.pc;
                        code = cF.code;
						lv=cF.lv;

                        continue;
                    }
                    //nf.code = m.getCode();
                    cF = nf;
                    pc = cF.pc;
                    code = cF.code;
					lv=cF.lv;

                    cC = m.getMyClass();
                    if (classa != null)
                        classa.setFrame(cF);
                    stack.push(cF);
                    if (fsa != null)
                        fsa.notifyDataSetChanged();
                    //cF.code = m.getCode();
                    if (ca != null) {
                        ca.setCode(cF);

                        ca.setPc(pc);
                    }
                    if (sa != null)
                        sa.setFrame(cF);
                    if (lva != null)
                        lva.setLv(cF);
                    continue;
                case (byte) 0xB7:
                    //invokespecial

                    m = cC.getMethod(ii(code[pc + 1], code[pc + 2]));
                    pc += 3;
                    cF.pc = pc;
                    System.out.println("invokespecial: " + m.getMyClass().getName() + "." + m.getNameAndDesc());
					
					nf = cF.newRefFrame(m);
                    if (m.isNative()) {
                        // m.invoke(cF.stack, this);
                        invokeNative(m.getMyClass().getName(), m.getNameAndDesc(), cF,nf);
                        cF = stack.peek();
                        pc = cF.pc;
                        code = cF.code;
						lv=cF.lv;
                        continue;
                    }
					cF=nf;
                    pc = cF.pc;
                    code = cF.code;
					lv=cF.lv;
                    cC = m.getMyClass();
                    if (classa != null)
                        classa.setFrame(cF);
                    stack.push(cF);
                    if (fsa != null)
                        fsa.notifyDataSetChanged();

                    if (ca != null) {
                        ca.setCode(cF);

                        ca.setPc(pc);
                    }
                    if (sa != null)
                        sa.setFrame(cF);
                    if (lva != null)
                        lva.setLv(cF);
                    continue;
                case (byte) 0xB8:
                    //invokestatic
                    if (fsa != null)
                        fsa.notifyDataSetChanged();
                    m = cC.getMethod(ii(code[pc + 1], code[pc + 2]));
                    pc += 3;
                    cF.pc = pc;

                    System.out.println("invokestatic: " + m.getMyClass().getName() + "." + m.getNameAndDesc());
					
					nf = cF.newFrame(m);
                    if (m.isNative()) {
                        invokeNative(m.getMyClass().getName(), m.getNameAndDesc(), cF,nf);
                        cF = stack.peek();
                        pc = cF.pc;
                        code = cF.code;
						lv=cF.lv;
                        continue;
                    }
                    cF=nf;
                    pc = cF.pc;
                    code = cF.code;
					lv=cF.lv;
                    cC = m.getMyClass();
                    if (classa != null)
                        classa.setFrame(cF);
                    stack.push(cF);
                    if (fsa != null)
                        fsa.notifyDataSetChanged();
                    // cF.code = m.getCode();
                    if (ca != null) {
                        ca.setCode(cF);

                        ca.setPc(pc);
                    }
                    if (sa != null)
                        sa.setFrame(cF);
                    if (lva != null)
                       	lva.setLv(cF);
                    continue;
                case (byte) 0xB9:
                    //invokeinterface

                    if (fsa != null)
                        fsa.notifyDataSetChanged();
                    m = cC.getMethod(ii(code[pc + 1], code[pc + 2]));
                    System.out.println("invokeinterface: " + m.getMyClass().getName() + "." + m.getNameAndDesc());

                    byte count = code[pc + 3];
                    byte zero = code[pc + 4];
                    pc += 5;
                    cF.pc = pc;
                    //if(count>1) {
                    nf = new Frame(m);
                   
					Object[] mlv=new Object[count];
                    for (int ix = 0; ix < count; ix++) {
                        //nf.getLv().put(p - ix - 1, cF.pop());
						mlv[count - ix - 1]=cF.pop();
                    }

                    //}
                    oref = (int) mlv[0];

                    ClassInstance ci = (ClassInstance) heap.get(oref);
                    AClass mc = ci.getMyClass();
                    m = mc.getMethod(m.getNameAndDesc());


                    if (m.isNative()) {
                        //m.invoke(cF.stack, this);
						throw new RuntimeException("check.that");
                        //continue;
                    }
					nf.lv=new Object[m.getMaxLocals()];
					
					for(int j=0;j<mlv.length;j++){
						nf.lv[j]=mlv[j];
					}
					
                    nf.setMethod(m);
                    //nf = cF.newRefFrame(m);
                    oref = (int) lv[0];
                    if (m.isAbstract()) {
                        m = ((Instance) heap.get(oref)).getMyClass().getMethod(m.getNameAndDesc());
                        if (m instanceof NativeMethod) {
                            ((NativeMethod) m).invoke(nf, this);
                            continue;
                        }
                    }
                    nf.code = m.getCode();
                    cF = nf;
                    pc = cF.pc;
                    code = cF.code;
					lv=cF.lv;
					
                    cC = m.getMyClass();
                    if (classa != null)
                        classa.setFrame(cF);
                    stack.push(cF);
                    if (fsa != null)
                        fsa.notifyDataSetChanged();
                    cF.code = m.getCode();
                    if (ca != null) {
                        ca.setCode(cF);

                        ca.setPc(pc);
                    }
                    if (sa != null)
                        sa.setFrame(cF);
                    if (lva != null)
                        lva.setLv(cF);
                    continue;
				case (byte)0xBA:
					//invokedynamic
					throw new RuntimeException("invokedynamic not implemented");
                case (byte) 0xBB:
                    //new

                    c = new ClassInstance(cC.getClass(i(code[pc + 1], code[pc + 2])));
                    // cC.getClass(i(code[pc + 1], code[pc + 2]));
                    int a = addToHeap(c);
                    cF.push(a);
                    pc += 3;
                    continue;
                case (byte) 0xBC:
                    //newarray
                    //i1=size
					i1 = (int) cF.pop();
                    array = new ArrayInstance(i1, code[pc + 1]);
                    aref = addToHeap(array);
                    cF.push(aref);
                    pc += 2;
                    continue;
                case (byte) 0xBD:
                    //anewarray
                    i1 = (int) cF.pop();
                    i(code[pc + 1], code[pc + 2]);
                    array = new ArrayInstance(i1, ArrayInstance.T_INT);
                    aref = addToHeap(array);
                    cF.push(aref);
                    pc += 3;
                    continue;
                case (byte) 0xBE:
                    //arraylength
                    aref = (int) cF.pop();
                    array = (ArrayInstance) heap.get(aref);
                    cF.push(((Object[]) array.a).length);
                    pc += 1;
                    continue;

                case (byte) 0xBF:
                    //athrow
                    oref = (int) cF.pop();
                    c = (ClassInstance)getFromHeap(oref);
					while (!stack.isEmpty())
					{
						MException e=cF.m.checkException(pc);
						boolean _catch=false;
						if (e != null)
						{
							cl = cC.getClass(e.catch_type);
							c = (ClassInstance)getFromHeap(oref);
							if (c.getClass().equals(cl))
								_catch = true;
						}
						if (!_catch)
						{
							
							last_frame=stack.pop();
							exceptionstack.push(last_frame);
							if (stack.isEmpty())
							{
								if (ca != null)
									ca.setCode(null);
								if (sa != null)
									sa.setFrame(null);
								if (lva != null)
									this.lva.setLv(null);
								throw new RuntimeException(c.getMyClass().getName());
								//return false;
							}
							cF = stack.peek();
							pc = cF.pc;
							code = cF.code;
							lv = cF.lv;
							cF.throwException(oref);
							//cF.push(oref);
							cC = cF.getMyClass();
							if (classa != null)
								classa.setFrame(cF);
							if (fsa != null)
								fsa.notifyDataSetChanged();

							// this.code=c.getMethod(11).attrs[0].code;
							if (ca != null)
							{
								ca.setCode(cF);
								ca.setPc(pc);
							}
							if (sa != null)
								sa.setFrame(cF);
							if (lva != null)
								lva.setLv(cF);
						}
						else
						{
							exceptionstack.clear();
							pc = e.handler_pc + 1;
							cF.catchException();
						}
					}


                    continue;
                case (byte) 0xC0:
                    //checkcast
                    fc = cC.getConstant(ii(code[pc + 1], code[pc + 2]));
                    fc = cC.getConstant(fc.index1);
                    if (!fc.str.startsWith("[")) {
                        cl = Class.getClass(fc.str);
                        i1 = (int) cF.peek();
                        obj = heap.get(i1);
                        if (obj instanceof ClassInstance)
                            if (!((ClassInstance) obj).getMyClass().isInstance(cl)) {
                                //ClassCastException
                                throw new RuntimeException("CLASSCASTEXCEPTION");
                            }
                    }
                    pc += 3;
                    continue;
                case (byte) 0xC1:
                    //instanceof
                    i1 = (int) cF.pop();
                    obj = heap.get(i1);
                    i1 = ii(code[pc + 1], code[pc + 2]);
                    cl = cC.getClass(i1);
                    if (cl.isInstance(obj))
                        cF.push(1);
                    else
                        cF.push(0);
                    pc += 3;
                    continue;
                case (byte) 0xC2:
                    //monitorenter
                    oref = (int) cF.pop();
                    pc += 1;
                    continue;
                case (byte) 0xC3:
                    //monitorexit
                    oref = (int) cF.pop();
                    pc += 1;
                    continue;

                case (byte) 0xC4:
                    //wide

                    pc += 1;
                    throw new RuntimeException("not implememted");
                    //continue;
				case (byte) 0xC5:
                    //multianewarray
					i1=i(code[pc + 1], code[pc + 2]);
					//dimensions
					i2=code[pc + 1];
                    pc += 4;
                    throw new RuntimeException("not implememted");
                    //continue;
                case (byte) 0xC6:
                    //ifnull
                    if ((int) cF.pop() == 0)
                        pc += i(code[pc + 1], code[pc + 2]);
                    else
                        pc += 3;
                    continue;
                case (byte) 0xC7:
                    //ifnonnull
                    if ((int) cF.pop() != 0)
                        pc += i(code[pc + 1], code[pc + 2]);
                    else
                        pc += 3;
                    continue;
                case (byte) 0xC8:
                    //goto

                    pc += i(code[pc + 1], code[pc + 2], code[pc + 3], code[pc + 4]);
                    continue;
				case (byte) 0xC9:
					//jsr_w
					cF.push(pc+5);
					pc += i(code[pc + 1], code[pc + 2], code[pc + 3], code[pc + 4]);
					continue;
                default:
                    pc += 1;
                    throw new UnsupportedOperationException("opcode:" + Integer.toHexString(code[pc]));
                    // cs+= byteToHex(code[i])+"\n";
            }
        } while (running);
        cF.pc = pc;
        return true;
    }

    private int l2i1(long l) {
        return (int) (l >> 32);
    }

    private int l2i2(long l) {
        return (int) l;
    }

    private void invokeNative(String name, String nameAndDesc, Frame cF,Frame nf) {
        Natives.invoke(name, nameAndDesc, cF,nf, this);
    }


    private static short i(byte b1, byte b2) {
        return (short) (((b1 & 0xff) << 8) | (b2 & 0xff));
    }

    private static long _2i2l(int i1, int i2) {
        return ((Integer) i2).longValue() << 32 | ((Integer) (i1)).longValue() & 0xFFFFFFFFL;
    }

    private static int ii(byte b1, byte b2) {
        return (((b1 & 0xff) << 8) | (b2 & 0xff));
    }

    private static int i(byte b1, byte b2, byte b3, byte b4) {
        return ((0xFF & b1) << 24) | ((0xFF & b2) << 16) |
                ((0xFF & b3) << 8) | (0xFF & b4);
    }

    private static long l(byte b1, byte b2, byte b3, byte b4, byte b5, byte b6, byte b7, byte b8) {
        return ((0xFFL & b1) << 56) | ((0xFFL & b2) << 48) |
                ((0xFFL & b3) << 40) | (0xFFL & b4 << 32) |
                ((0xFFL & b5) << 24) | (0xFFL & b6 << 16) |
                ((0xFFL & b7) << 8) | (0xFFL & b8 << 0);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(stack.peek().toString());
        return sb.toString();
    }

    public Frame getCurrentFrame() {
        return stack.peek();
    }
}
