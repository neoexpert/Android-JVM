package com.neoexpert.jvm;

import com.neoexpert.jvm._abstract.*;
import java.util.*;

public class Frame
{
	private int exception=0;

	public Object get(int pos)
	{
		return stack[pos];
	}

	public Object peek()
	{
		return stack[stackpos-1];
	}

	public void catchException()
	{
		exception=0;
	}

	public AMethod getMethod()
	{
		return m;
	}

	
	public int getCurrentLine()
	{
		return m.getCurrentLine(pc);
	}
	
	public void throwException(int oref){
		this.exception=oref;
	}
	
	public boolean isThrowing()
	{
		return exception!=0;
	}
	
	@Override
	public String toString()
	{
		return m.getMyClass().getName()+"."+ m.getNameAndDesc()+ ": "+getCurrentLine();
	}
	
	public int pc=0;
	public byte[] code;
	//local variables
	//private HashMap<Integer,Object> lv=new HashMap<>();
	//op-stack
	int stackpos=0;
	public Object[] stack;
	public void push(Object v){
		stack[stackpos++]=v;
	}
	
	public Object pop(){
		return stack[--stackpos];
	}
	
	public Object[] lv;

	public AMethod m;
	private Class c;
	public AClass getMyClass(){
		return m.getMyClass();
	}
	
	public Frame(int lvsize,int stacksize){
		lv=new Object[lvsize];
		stack=new Object[stacksize];
	}
	public Frame(AMethod m){
		this.m=m;
		stack=new Object[m.getMaxStack()];
		//this.consts=cp;
		code=m.getCode();
		if (code != null)
		{
			lv = new Object[m.getMaxLocals()];
		}
	}

	
	public Frame newRefFrame(AMethod m)
	{
		Frame f=new Frame(m);
		f.code=m.getCode();
		int p=m.getParamsCount()+1;
		ArrayList<Object> lv=new ArrayList<>();
		for(int i=0;i<p;i++)
		{
			//f.getLv().put(p-i-1,stack.pop());
			lv.add(0,pop());
			if(i<p-1)
			switch(m.args.get(m.args.size()- i-1)){
				case "J":
				case "D":
					lv.add(0,pop());
					//f.getLv().put(p-i-2,stack.pop());
			}
			
		}
		if(f.lv==null){
			f.lv=new Object[lv.size()];
		}
		for(int i=0;i<lv.size();i++)
			f.lv[i]=lv.get(i);
		
		
		return f;
	}

	public Frame newFrame(AMethod m)
	{
		Frame f=new Frame(m);
		f.code=m.getCode();
		int p=m.getParamsCount();
		ArrayList<Object> lv=new ArrayList<>();
		for(int i=0;i<p;i++)
		{
			//f.getLv().put(p-i-1,stack.pop());
			lv.add(0,pop());
			if(i<p)
				switch(m.args.get(m.args.size()- i-1)){
					case "J":
					case "D":
						lv.add(0,pop());
						//f.getLv().put(p-i-2,stack.pop());
				}

		}
		if(f.lv==null)
		{
			f.lv=new Object[lv.size()];
		}
		for(int i=0;i<lv.size();i++)
			f.lv[i]=lv.get(i);


		return f;
	}

/*
	public Frame newFrame(AMethod m)
	{
		Frame f=new Frame(m);
		int p=m.getParamsCount();
		int pos=0;
		for(int i=0;i<p;i++)
		{
			f.getLv().put(p-i-1,stack.pop());
			switch(m.args.get(i)){
				case "J":
				case "D":
					lv.add(0,stack.pop());
					//f.getLv().put(p-i-2,stack.pop());
			}
		}
		return f;
	}

 */

	

	

	public void setMethod(AMethod m) {
		this.m=m;
		code=m.getCode();
		stack=new Object[m.getMaxStack()];
	}
	public void initLocals(AMethod m) {
		this.m=m;
		code=m.getCode();
		Object[] oldlv = lv;
		lv = new Object[m.getMaxLocals()];
		for(int i=0;i<oldlv.length;i++)
			lv[i]=oldlv[i];
	}

	//int l0, l1,l2,l3;
	
	
	
}
