package com.neoexpert.jvm;
import com.neoexpert.jvm._abstract.*;
import com.neoexpert.jvm.method.*;
import java.nio.*;

public class Field extends AField
{

	@Override
	public String getDesc()
	{
		return desc;
	}


	@Override
	public String parseType()
	{
		String type=desc;
		
		if(type.charAt(0)=='['){
			return parseType(type.substring(1)+"[]");
		}
		return parseType(type);
		
	}
	
	public String parseType(String type)
	{
		if(type.length()==1){
			return parsePrimitiveType(type);
		}
		if(type.charAt(0)=='L')
			return type.substring(1,type.indexOf(";"));
		
		return type;
	}
	private String parsePrimitiveType(String type)
	{
		switch(type){
			case "I":return "int";
			case "Z":return "boolean";
			case "B":return "byte";
			case "C":return "char";
			case "D":return "double";
			case "F":return "float";
			case "J":return "long";
			case "S":return "short";
			default: throw new RuntimeException("unknown primitive: "+type);
		}
	}


	@Override
	public String parseModifiers()
	{
		StringBuilder sb=new StringBuilder();

		if(isPublic())
			sb.append("public ");
		if(isPrivate())
			sb.append("private ");
		if(isProtected())
			sb.append("protected ");
		if(isStatic())
			sb.append("static ");
		if(isFinal())
			sb.append("final ");
		if(isVolatile())
			sb.append("volatile ");
		if(isTransient())
			sb.append("transient ");
		if(isSynthetic())
			sb.append("synthetic ");
		if(isEnum())
			sb.append("enum ");
		return sb.toString();
	}
	
	@Override
	public int getType()
	{
		if(desc.startsWith("L")){
			String classname=desc.substring(1,desc.indexOf(";"));
			return Class.getClass(classname,false).getMyInstanceRef();
		}
		if(desc.startsWith("[")){
			return Class.getArrayClass(desc);
		}
		return 0;
	}


	private Class c;

	@Override
	public AClass getMyClass()
	{
		return c;
	}


	@Override
	public int getModifiers()
	{
		return access_flags;
	}


	private byte tag;

	private short id;

	private String name2;

	@Override
	public void setTag(byte tag)
	{
		this.tag=tag;
	}

	@Override
	public void setId(short id)
	{
		this.id=id;
	}
	

	@Override
	public byte getTag()
	{
		return tag;
	}

	@Override
	public short getId()
	{
		return id;
	}
	
	
	private int access_flags;

	private char name_index;

	private char descriptor_index;

	private char attributes_count;

	private Attribute[] attrs;

	private String name;

	private String desc;
//	field_info {
//		u2             access_flags;
//		u2             name_index;
//		u2             descriptor_index;
//		u2             attributes_count;
//		attribute_info attributes[attributes_count];
//	}
	private static int i(byte b1, byte b2)
	{
		return (((b1 & 0xff) << 8) | (b2 & 0xff));
	}
	public Field(){}
	public Field(ByteBuffer buf, Class c){
		this.c=c;
		this.tag=CFieldRef;
		access_flags=buf.getChar();
		name_index=buf.getChar();
		
		name=c.getConstant(name_index).str;
		
		int di=buf.getChar();
		//int di=(int)descriptor_index;
		
		desc=c.getConstant(di).str;
		
		
		attributes_count=buf.getChar();
		attrs=new Attribute[attributes_count];
		for(int ix=0; ix<attributes_count; ix++){
			Attribute a=new Attribute(buf);

			 name2=c.getConstant(a.attribute_name_index-1).str;
			if(name.equals("Code"))
				a=new CodeAttribute(a,c);
			attrs[ix] = a;
		}
	}

	public void setName(String name)
	{
		this.name=name;
	}
	
	
	
	public String getName()
	{
		return name;
	}
	public String getNameAndDesc()
	{
		return name+desc;
	}
	public static final int ACC_PUBLIC	=0x0001;//	Declared public; may be accessed from outside its package.
	public static final int  ACC_PRIVATE=	0x0002;//	Declared private; usable only within the defining class.
	public static final int  ACC_PROTECTED	=0x0004;//	Declared protected; may be accessed within subclasses.
	public static final int  ACC_STATIC	=0x0008;//	Declared static.
	public static final int  ACC_FINAL	=0x0010;//	Declared final; never directly assigned to after object construction (JLS §17.5).
	public static final int  ACC_VOLATILE	=0x0040;//	Declared volatile; cannot be cached.
	public static final int  ACC_TRANSIENT	=0x0080;//	Declared transient; not written or read by a persistent object manager.
	public static final int  ACC_SYNTHETIC	=0x1000;//	Declared synthetic; not present in the source code.
	public static final int  ACC_ENUM	=0x4000;//	Declared
	public boolean isPublic()
	{
		return (access_flags&ACC_PUBLIC)!=0;
	}
	public boolean isPrivate()
	{
		return (access_flags&ACC_PRIVATE)!=0;
	}
	public boolean isProtected()
	{
		return (access_flags&ACC_PROTECTED)!=0;
	}
	public boolean isStatic()
	{
		return (access_flags&ACC_STATIC)!=0;
	}
	public boolean isFinal()
	{
		return (access_flags&ACC_FINAL)!=0;
	}
	public boolean isVolatile()
	{
		return (access_flags&ACC_VOLATILE)!=0;
	}
	public boolean isTransient()
	{
		return (access_flags&ACC_TRANSIENT)!=0;
	}
	public boolean isSynthetic()
	{
		return (access_flags&ACC_SYNTHETIC)!=0;
	}
	public boolean isEnum()
	{
		return (access_flags&ACC_ENUM)!=0;
	}
	
	
	@Override
	public String toString()
	{
		String s=parseModifiers();
		s+="\nField: "+name+"\n";
		s+="access_flags: "+access_flags+"\n";
		s+="name_index: "+(int)name_index+"\n";
		s+="descriptor_index: "+(int)descriptor_index+"\n";
		s+="attributes_count: "+(int)attributes_count+"\n";
		for(int ix=0; ix<attributes_count; ix++){
			s+= "attribute "+ix+"\n";
			s+=attrs[ix].toString();
		}
		return s+"\n";
	}
	
}
