package com.neoexpert.jvm;
import com.neoexpert.jvm._abstract.*;
import java.util.*;

public class ClassInstance implements Instance
{
    public Object cl;
    Object ni;

	public HashMap<String,Object> getFields()
	{
		return fields;
	}

	public void putField(String name, Object ic)
	{
		fields.put(name,ic);
	}



	public Object getNativeInstance()
	{
		return ni;
	}

	public void setNativeInstance(Object ni)
	{
		if(this.ni==null)
			this.ni=ni;
	}

	public Object getField(String name)
	{
		Object f = fields.get(name);
		if(f==null)
			return 0;
		return f;
	}
	

	@Override
	public AClass getMyClass()
	{
		return c;
	}
	

	private AClass c;
	public ClassInstance(AClass c){
		this.c=c;
	}
	private HashMap<String,Object> fields=new HashMap<>();

	 
	
}
