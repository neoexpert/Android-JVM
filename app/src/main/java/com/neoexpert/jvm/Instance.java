package com.neoexpert.jvm;

import com.neoexpert.jvm._abstract.*;

public interface Instance 
{
	public AClass getMyClass();
}
