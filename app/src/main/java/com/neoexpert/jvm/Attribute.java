package com.neoexpert.jvm;
import java.nio.*;

public class Attribute
{
	public Attribute(){}
	//	attribute_info {
//		u2 attribute_name_index;
//		u4 attribute_length;
//		u1 info[attribute_length];
//	}
	public Attribute(ByteBuffer buf){
		attribute_name_index=buf.getChar();
		//log("attribute_name_index: "+Integer.toHexString(af1));
		attribute_length=buf.getInt();
		info=new byte[attribute_length];
		//log("attribute_length: "+Integer.toHexString(attribute_length));
		for(int ix=0; ix<attribute_length; ix++){
			info[ix]=buf.get();

		}
		
		//info=new byte[attribute_length];
	}
	public int attribute_name_index;
	public int attribute_length;
	public byte[] info;

	@Override
	public String toString()
	{
		String s="";
		s+="attribute_name_index: "+(int)attribute_name_index+"\n";
		s+="attribute_length: "+attribute_length+"\n";
		
		return s+bytesToHex(info);
	}
	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
	public static String bytesToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		for ( int j = 0; j < bytes.length; j++ ) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}
	
}
