package com.neoexpert.jvm;


import com.neoexpert.jvm._abstract.*;
import com.neoexpert.jvm.constants.*;

import java.io.*;
import java.nio.*;
import java.util.*;
import java.util.zip.*;
import com.neoexpert.jvm.natives.*;
import com.neoexpert.jvm.method.*;

public class Class extends AClass
{

	

	static Map<String,Integer> array_classes=new HashMap<>();
	static Map<String,Integer> primitive_classes=new HashMap<>();

	public static int getPrimitiveClass(String classname)
	{
		switch(classname){
			case "I":
			case "Z":
			case "B":
			case "C":
			case "D":
			case "F":
			case "J":
			case "S":
				Integer oref=primitive_classes.get(classname);
				if(oref==null){
					AClass cl=getClass("java/lang/Class");
					ClassInstance ci=new ClassInstance(cl);
					oref=MyThread.addToHeap(ci);
					ci.putField("name", classname);
					primitive_classes.put(classname,oref);
				}
				return oref;
		}
		throw new RuntimeException("wrong primitive class");
	}
	public static int getArrayClass(String a)
	{
		Integer oref=array_classes.get(a);
		if(oref==null){
			AClass cl=getClass("java/lang/Class");
			ClassInstance ci=new ClassInstance(cl);
			oref=MyThread.addToHeap(ci);
			ci.putField("name", a);
			ci.cl=a;
			array_classes.put(a,oref);
		}
		return oref;
	}
	

	@Override
	public boolean isInterface()
	{
		return super.isInterface();
	}

	
	
	public void putStatic(String fname, Object obj)
	{
		staticfields.put(fname,obj);
	}
	@Override
	public void putStatic(int id, Object obj)
	{
		Constant c=getConstant(id);
		Constant classref = getConstant(c.index1);
		Constant cls = getConstant(classref.index1);
		if (cls.str.equals(this.name)) {
			//nameandtyperef
			Constant ntref = getConstant(c.index2);
			Constant fname = getConstant(ntref.index1);
			Constant ftype = getConstant(ntref.index2);
			staticfields.put(fname.str,obj);


		} else {
			throw new RuntimeException();
		}
	}
	

	@Override
	public boolean isInitialized()
	{
		return initialized;
	}


    private int myoref;


	private boolean initialized;

	public static AClass getClass(String classname)
	{
		return getClass(classname,true);
	}

    @Override
    public Iterator<Constant> constantsIterator() {
        return constants.iterator();
    }


    ArrayList<Constant> constants = new ArrayList<>();


    public void addConstant(Constant c) {
        constants.add(c);
    }

    public Constant getConstant(int i) {
        try {
            return constants.get(i - 1);
        } catch (IndexOutOfBoundsException e) {
            throw new RuntimeException("wrong index, class: " + name);
        }
    }

    @Override
    public boolean isInstance(Object obj) {
        if (obj instanceof ClassInstance)
            return isInstance(((ClassInstance) obj).getMyClass());

        return false;
    }

    @Override
    public boolean isInstance(AClass cl) {
        if (cl.getName().equals(name))
            return true;
        for(int i:interfaces){
            AClass iface = getClass(i);
            if(cl.getName().equals(iface.getName()))
                return true;
        }
        AClass scl = getSuperClass();
        if(scl!=null)
            return scl.isInstance(cl);
        return false;
    }


    public static void reset() {
        classes.clear();
    }

    @Override
    public HashMap<String, Object> getStaticFields() {
        return staticfields;
    }

    private static HashMap<String, AClass> classes = new HashMap<>();
    String name;
    public static final int HEAD = 0xcafebabe;
    public HashMap<Integer, AMethod> methods = new HashMap<>();

    public ArrayList<AMethod> inits = new ArrayList<>();


    private HashMap<String, Object> staticfields = new HashMap<>();
    private int constant_pool_count;

    private int interfaces_count;
    private int[] interfaces;

    private int fields_count;

    private int methods_count;

    private int attributes_count;

    private int super_class;

    private int this_class;

    


    //	ClassFile {
//		u4             magic;
//		u2             minor_version;
//		u2             major_version;
//		u2             constant_pool_count;
//		cp_info        constant_pool[constant_pool_count-1];
//		u2             access_flags;
//		u2             this_class;
//		u2             super_class;
//		u2             interfaces_count;
//		u2             interfaces[interfaces_count];
//		u2             fields_count;
//		field_info     fields[fields_count];
//		u2             methods_count;
//		method_info    methods[methods_count];
//		u2             attributes_count;
//		attribute_info attributes[attributes_count];
//	}
    public Class() {

    }

	@Override
    public int getFieldId(String fname) {
        AField f = fields.get(fname);
        if (f == null)
            return getSuperClass().getFieldId(fname);
        return f.getId();
    }

    @Override
    public void getConstantInstance(int id, Frame cF) {
        Constant c = getConstant(id);
		int i1,i2;
        switch (c.getTag()) {
            case Constant.CString:
				
                c = getConstant(c.index1);
                
				int oref= NString.createInstance(c.str);
				cF.push(oref);
                return;
            case Constant.CFloat:

				cF.push(c.fvalue);
                return ;
			
            case Constant.CInteger:
				cF.push(c.ivalue);
                return ;
            case Constant.CLong:
				i1 = (int)(c.lvalue >> 32);
				i2 = (int)c.lvalue;
				cF.push(i1);
				cF.push(i2);
                return; 
            case Constant.CDouble:
				long l=Double.doubleToRawLongBits(c.dvalue);
				i1 = (int)(l>> 32);
				i2 = (int)l;
				cF.push(i1);
				cF.push(i2);
				double d=Double.longBitsToDouble(l);
                return ;
            case Constant.CClass:
                Constant cref = getConstant(c.index1);
                System.out.println(cref);
				String cname=cref.str;
				if(cname.charAt(0)=='['){
					oref=Class.getArrayClass(cname);
					cF.push(oref);
					return;
				}
                oref=getClass(cref.str,false).getMyInstanceRef();
				cF.push(oref);
				
                return;
        }
    }

	

    public AMethod getMyMethod(String nameAndDesc) {
        return methodsbyname.get(nameAndDesc);
    }


    public AClass getClass(byte id) {
        if (id == 0)
            return this;
        return null;
    }

    public AClass getSuperClass() {
        if(super_class==0)
            return null;
        Constant c = getConstant(super_class);
        c = getConstant(c.index1);
        return getClass(c.str);
    }

    @Override
    public int getMyInstanceRef() {
        return myoref;
    }

    @Override
    public void getStatic(int i2, Frame cF) {
		Constant fref = getConstant(i2);
		Constant ntref = getConstant(fref.index2);
		Constant fnref = getConstant(ntref.index1);
		Constant ftref = getConstant(ntref.index2);
		
        if (fref.index1 == this_class)
		{
            getStatic(fnref.str,ftref.str,cF);
		}
		else
		{
			Constant cref = getConstant(fref.index1);

			String classname = getConstant(cref.index1).str;
			AClass c = getClass(classname);


			if (cref.index1 == this_class)
				getStatic(fnref.str, ftref.str,cF);
			else
				c.getStatic(fnref.str, ftref.str,cF);
		}
    }

    @Override
    public void getStatic(String name, String desc, Frame cF) {
        Object obj = staticfields.get(name);
        switch(desc){
            case "J":
                long l= (long) obj;
                cF.push(l2i1(l));
                cF.push(l2i2(l));
                break;
            case "D":
                l= (long) obj;
                cF.push(l2i1(l));
                cF.push(l2i2(l));
                break;
            default:
                cF.push(obj);
                break;
        }
    }
	
	@Override
	public Object getStatic(String name, String desc)
	{
		return staticfields.get(name);
	}
    private int l2i1(long l) {
        return (int) (l >> 32);
    }

    private int l2i2(long l) {
        return (int) l;
    }

	
	public static ArrayList<String> jars=new ArrayList<>();
	public static void addJar(String jar)
	{
		jars.add(classpath + "/"+jar);
	}
    public static AClass getClass(String classname,boolean init) {
        AClass c = classes.get(classname);
        if (c != null){
			if(init&&!c.isInitialized())
				c.clinit();
            return c;
		}
//		try
//		{
//			java.lang.Class jc=java.lang.Class.forName(classname.replaceAll("/", "."));
//			if(jc!=null){
//				c=new NativeClass(jc);
//				classes.put(classname,c);
//				return c;
//			}
//		}
//		catch (ClassNotFoundException e)
//		{
//			//throw new RuntimeException(e);
//		}

        final File file = new File(classpath, classname + ".class");
        if (file.exists())
            try {
                InputStream raw = new FileInputStream(file);
                return new Class(raw,init);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
		for (String jar:jars)
		{
			try
			{

				ZipFile zipFile = new ZipFile(jar);

				ZipEntry e = zipFile.getEntry(classname + ".class");
				if (e == null)
				{
					continue;
				}
				int size = zipFile.size();
				InputStream is = zipFile.getInputStream(e);
				return new Class(is, init);
			}
			catch (Exception e)
			{
				throw new RuntimeException(e);
			}
		}
		
		throw new RuntimeException(classname + " class not found");
    }


    public static String classpath;


    public Class(InputStream raw, boolean init) throws Exception {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        int nRead;
        byte[] data = new byte[16384];

        while ((nRead = raw.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, nRead);
        }

        byte[] bytes = buffer.toByteArray();


        ByteBuffer buf = ByteBuffer.wrap(bytes);

        if (buf.order(ByteOrder.BIG_ENDIAN).getInt() != HEAD) {
            throw new Exception("not a valid class file");
        }
        int minor = buf.getChar(), ver = buf.getChar();
        log("version " + ver + '.' + minor);
        constant_pool_count = buf.getChar();
        log("constant_pool_count: " + (int) constant_pool_count);

        for (int ix = 1; ix < constant_pool_count; ix++) {
            Constant c = new Constant((short)ix, buf);
            if (c.getTag() == ClassItem.CLong || c.getTag() == ClassItem.CDouble) {
                ix++;
                constants.add(c);
            }


            log(ix + ": " + c.toString());
            constants.add(c);
            //constants[ix-1] = c;
            //byte tag = buf.get();
//			switch (tag)
//			{
//				default:
//					log("unknown pool item type " + buf.get(buf.position() - 1));
//					return;
//				case CONSTANT_Utf8: decodeString(ix, buf); continue;
//				case CONSTANT_Class: case CONSTANT_String: case CONSTANT_MethodType:
//					s = "%d:\t%s ref=%d%n"; index1 = buf.getChar();
//					break;
//				case CONSTANT_FieldRef: case CONSTANT_MethodRef:
//				case CONSTANT_InterfaceMethodRef: case CONSTANT_NameAndType:
//					s = "%d:\t%s ref1=%d, ref2=%d%n";
//					index1 = buf.getChar(); index2 = buf.getChar();
//					break;
//				case CONSTANT_Integer: s = "%d:\t%s value=" + buf.getInt() + "%n"; break;
//				case CONSTANT_Float: s = "%d:\t%s value=" + buf.getFloat() + "%n"; break;
//				case CONSTANT_Double: s = "%d:\t%s value=" + buf.getDouble() + "%n"; break;
//				case CONSTANT_Long: s = "%d:\t%s value=" + buf.getLong() + "%n"; break;
//				case CONSTANT_MethodHandle:
//					s = "%d:\t%s kind=%d, ref=%d%n"; 
//					index1 = buf.get(); 
//					index2 = buf.getChar();
//					break;
//				case CONSTANT_InvokeDynamic:
//					s = "%d:\t%s bootstrap_method_attr_index=%d, ref=%d%n";
//					index1 = buf.getChar(); 
//					index2 = buf.getChar();
//					break;
//			}


        }
        access_flags = buf.getChar();

        log("access_flags: " + Integer.toHexString(access_flags));
        this_class = buf.getChar();

        log("this_class: " + (int) this_class);
        name = getConstant(getConstant((int) this_class).index1).str;
        super_class = buf.getChar();


        log("super_class: " + (int) (super_class));
        interfaces_count = buf.getChar();
        log("interfaces_count: " + Integer.toHexString(interfaces_count));
        interfaces = new int[interfaces_count];
        for (int ix = 0; ix < interfaces_count; ix++) {
            interfaces[ix] = buf.getChar();
            //AClass i = getClass(interfaces[ix]);

        }


        fields_count = buf.getChar();
        log("");
        log("fields_count: " + Integer.toHexString(fields_count));
        //if("".equals(""))
        //return;
        for (int ix = 0; ix < fields_count; ix++) {
            Field f = new Field(buf, this);
            fields.put(f.getName(), f);
            log("field: " + ix + ":");
            log(f.toString());
        }
        methods_count = buf.getChar();
        log("methods_count: " + Integer.toHexString(methods_count));
        log("");

        AMethod[] methods = new AMethod[methods_count];
        for (int ix = 0; ix < methods_count; ix++) {
            log("method " + ix + ":");
			AMethod m;
            try {
               m  = new Method(buf, this);
                methodsbyname.put(m.getNameAndDesc(), m);
                methods[ix] = m;
                log(m.toString());
                if (m.getNameAndDesc().equals("<clinit>()V")) {
                    this.methods.put(0, m);
                }
                if (m.getNameAndDesc().equals("main([Ljava/lang/String;)V")) {
                    this.methods.put(1, m);
                }
                if (m.getName().contains("<init>")) {
                    inits.add(m);
                }
            } catch (Exception e) {
                log(e + "");
            }
            log(ix + "");
        }
        attributes_count = buf.getChar();
        log("attributes_count: " + (int) attributes_count);
        for (int ix = 0; ix < attributes_count; ix++) {
            log("attr " + ix + ":");
            Attribute a = new Attribute(buf);
            log(a.toString());
        }
        while (buf.hasRemaining())
            log(buf.position() + " -> " + buf.get());

        Iterator<Constant> it = constants.iterator();
        int i = 1;
        while (it.hasNext()) {
            Constant c = it.next();
            switch (c.getTag()) {
                case Constant.CLong:
                case Constant.CDouble:
                    it.next();
                    i++;
                    break;
                case Constant.CMethodRef:
                    String name = getMethodName(i);
                    for (int j = 0; j < methods.length; j++) {
                        if (name.equals(methods[j].getNameAndDesc())) {
                            //this.methods.put(i,methods[j]);
                        }
                    }
                    break;
                case Constant.CFieldRef:
                    Constant classref = getConstant(c.index1);
                    Constant cls = getConstant(classref.index1);
                    if (cls.str.equals(this.name)) {
                        //nameandtyperef
                        Constant ntref = getConstant(c.index2);
                        Constant fname = getConstant(ntref.index1);
                        Constant ftype = getConstant(ntref.index2);
                        AField f = fields.get(fname.str);

                        if (f != null) {
                            f.setId((short)i);
                            name = f.getName();

                            if (f.isStatic()) {
                                switch (ftype.str){
                                    case "J":
                                        staticfields.put(name, 0L);
                                        break;
                                    case "D":
                                        staticfields.put(name, 0L);
                                        break;
                                    default:
                                        staticfields.put(name, 0);
                                        break;

                                }
                            }

                        }
                    } else {
                        //Class nc=Class.getClass(cls.str);
                    }

                    break;

            }
            i++;
        }
        classes.put(name, this);
		initialized=init;
        myoref=new MyThread(Class.getClass("java/lang/Class"),"class_init",null).newInstance("<init>(Ljava/lang/ClassLoader;)V",0);
        ClassInstance ci = (ClassInstance) MyThread.getFromHeap(myoref);
        ci.cl=this;
		if(init)
        	clinit();
		
    }

	public MyThread createInitMethod(){
		return new MyThread(this, "clinit",null)
			.setMethodToRun("<clinit>()V");
	}
	@Override
	public void clinit(){
        System.out.println("calling clinit for "+name);
		
		MyThread t=createInitMethod();
		try{
			t.run();
		}
		catch(Exception e){
			throw new RuntimeException(t.getStackTrace(),e);
		}
	}
	
    public boolean hasMainMethod() {
        return methods.get(1) != null;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getMethodName(int id) {
        Constant mc = getConstant(id);
        int cref = mc.index1;
        int mref = mc.index2;
        int nid = getConstant(mref).index1;
        int tid = getConstant(mref).index2;
        //id=constants.getConstant(ntref-1).index1;
        String mname = getConstant(nid).str;
        mname += getConstant(tid).str;
        return mname;
    }

    public AMethod getMyMethod(int id) {
        Constant mc = getConstant(id);
        int cref = mc.index1;
        int mref = mc.index2;
        //int nid = cp.getConstant(mref - 1).index1;
        //int tid = cp.getConstant(mref - 1).index2;
        //id=constants.getConstant(ntref-1).index1;
        //String mname=cp.getConstant(nid - 1).str;
        //mname+=cp.getConstant(tid - 1).str;

        if (cref == this_class) {
            return methods.get(id);
        } else {
            int nid = getConstant(mref).index1;
            int tid = getConstant(mref).index2;
            //id=cp.getConstant(ntref-1).index1;
            String mname = getConstant(nid).str;
            String parameters = getConstant(tid).str;
            AMethod m = methodsbyname.get(mname);
            if (m != null)
                return m;

            return getClass(cref).getMethod(mname, parameters, id - 1);
        }
    }

    public AMethod getMethod(int id) {
        Constant mc = getConstant(id);
        int cref = mc.index1;
        int mref = mc.index2;
        //int nid = cp.getConstant(mref - 1).index1;
        //int tid = cp.getConstant(mref - 1).index2;
        //id=constants.getConstant(ntref-1).index1;
        //String mname=cp.getConstant(nid - 1).str;
        //mname+=cp.getConstant(tid - 1).str;

        if (cref == this_class) {
            int nid = getConstant(mref).index1;
            int tid = getConstant(mref).index2;
            //id=cp.getConstant(ntref-1).index1;
            String mname = getConstant(nid).str;
            String parameters = getConstant(tid).str;
            return getMethod(mname + parameters);
        } else {
            int nid = getConstant(mref).index1;
            int tid = getConstant(mref).index2;
            //id=cp.getConstant(ntref-1).index1;
            String mname = getConstant(nid).str;
            String parameters = getConstant(tid).str;

            return getClass(cref).getMethod(mname, parameters, id - 1);
        }
    }

    @Override
    public AMethod getMethod(String nameAndDesc) {
        AMethod m = getMyMethod(nameAndDesc);
        if (m != null)
            return m;
        AClass sc = getSuperClass();
        if(sc==null)return null;
        return sc.getMethod(nameAndDesc);
    }

    @Override
    public AMethod getMethod(String mname, String parameters, int id) {
        AMethod m = methodsbyname.get(mname + parameters);
        if (m != null)
            return m;
		/*Iterator<Method> it;
		 if(mname.contains("<init>")){
			it=inits.iterator();
		}
		else
		 it=methods.values().iterator();
		while (it.hasNext())
		{
			Method m=it.next();
			if (m.getNameAndDesc().equals(mname))
				return m;
		}
		*/
        return getSuperClass().getMethod(mname, parameters, id);
    }


    public AClass getClass(int cref) {
        Constant c = getConstant(cref);
        c = getConstant(c.index1);
        String s = c.str;
        return getClass(s);
    }

    public AMethod getCLInitMethod() {
        return methods.get(0);
    }

    public AMethod getMainMethod() {
        return methods.get(1);
    }
//	method_info {
//		u2             access_flags;
//		u2             name_index;
//		u2             descriptor_index;
//		u2             attributes_count;
//		attribute_info attributes[attributes_count];
//	}

//	attribute_info {
//		u2 attribute_name_index;
//		u4 attribute_length;
//		u1 info[attribute_length];
//	}


    void log(String t) {
        s += t + "\n";
    }

    String s = "";

    @Override
    public String toString() {
        return name + ":\n" + s;
    }

}
