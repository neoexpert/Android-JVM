package com.neoexpert.ui;

import android.content.*;
import android.graphics.*;
import android.view.*;
import android.widget.*;
import com.neoexpert.jvm.*;
import com.neoexpert.jvm._abstract.*;
import com.neoexpert.jvm.adapter.*;
import com.neoexpert.jvm.constants.*;
import java.util.*;

public class ClassLVAdapter extends BaseAdapter 
implements ClassAdapter
{
	private Context context;
	ArrayList<Object> entrys=new ArrayList<>();

	private AClass c;

	private boolean active=true;

	public void clear()
	{
		entrys.clear();
		notifyDataSetChanged();
	}

	public Constant getConstant(int i)
	{
		return c.getConstant(i);
	}
	public void addClassItem(ClassItem ci){
		entrys.add(ci);
		notifyDataSetChanged();
	}
	public void setFrame(Frame f)
	{
//		if(active){
//			tv.setText("");
//			jvmdebug(cl.toString());
//		}
		entrys.clear();
		notifyDataSetChanged();
		this.c = f.getMethod().getMyClass();
		if(f.getMethod().isStatic())
			ci=null;
		else
			ci=(ClassInstance) MyThread.getFromHeap(f.lv[0]);

		Iterator<Constant> it=c.constantsIterator();
//		while (it.hasNext()&&active)
//		{
//			Constant c=it.next();
//
//			switch (c.getTag())
//			{
//				case Constant.CClass:
//					//case Constant.CFieldRef:
//					//case Constant.CMethodRef:
//				case Constant.CString:
//					entrys.add(c);
//					notifyDataSetChanged();
//			}
//		}

//		Iterator<AMethod> mit=c.methodsbyname.values().iterator();
//		while(mit.hasNext()&&active){
//			addClassItem(mit.next());
//		}
		Iterator<AField> fit=c.fields.values().iterator();
		while (fit.hasNext()&&active)
		{
			addClassItem(fit.next());
		}


	}
	ClassInstance ci;
	public ClassLVAdapter(Context context) {
		this.context = context;

	}

	@Override
	public int getCount() {
		return entrys.size();
	}

	@Override
	public Object getItem(int position) {
		return entrys.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) 
				context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(
				R.layout.constant, null);
		}

		TextView name = (TextView)convertView.findViewById(R.id.opTV);
		ClassItem c=(ClassItem)getItem(position);
		String text;
		if(c instanceof Constant)
			text=c.getId()+" => "+resolveConstant((Constant)c);
		else text=c.toString();
		//name.setText(text);
		switch(c.getTag()){
			case ClassItem.CClass:
				convertView.setBackgroundColor(Color.parseColor("#00AA00"));
				break;
			case ClassItem.CFieldRef:
				convertView.setBackgroundColor(Color.parseColor("#0000AA"));
				AField f=(AField) c;
				name.setText(f.parseModifiers());
				name.append(f.parseType());
				name.append(" ");
				name.append(f.getName());
				if(f.isStatic())
				{
					name.append(" = "+this.c.getStatic(f.getName(),f.getDesc())+"");
				}
				else{
					if(ci!=null)
					{
						name.append(" = "+ci.getField(f.getName()));
					}
				}
				break;
			case ClassItem.CMethodRef:
				convertView.setBackgroundColor(Color.parseColor("#AAAA00"));
				break;
			case ClassItem.CString:
				convertView.setBackgroundColor(Color.parseColor("#00AAAA"));
				break;
		}

		return convertView;
	}

	private String resolveConstant(Constant c)
	{
		String text=c.toString();
		if(c.index1>=0)
			text+="\n"+resolveConstant(this.c.getConstant(c.index1));
		if(c.index2>=0)
			text+="\n"+resolveConstant(this.c.getConstant(c.index2));

		return text;
	}
}
