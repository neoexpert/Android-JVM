package com.neoexpert.ui;

import android.content.*;
import android.graphics.*;
import android.view.*;
import android.widget.*;
import com.neoexpert.jvm.*;
import java.io.*;
import java.util.*;
import com.neoexpert.jvm._abstract.*;
import java.util.Map.*;

public class FieldsLVAdapter extends BaseAdapter
{
	private Context context;
	ArrayList<String> lines;
	

	private int green=Color.parseColor("#009900");

	public void setFields(MyThread t)
	{
		Frame cF=t.getCurrentFrame();
		if(cF.getMethod().isStatic())
			return;
		int oref=(int)cF.lv[0];
		if(oref==0)
			return;
		ClassInstance ci=(ClassInstance)t.getFromHeap(oref);
		HashMap<String, Object> fields=ci.getFields();
		for(Map.Entry<String, Object>e:  fields.entrySet()){
			lines.add(e.getKey()+ " = "+e.getValue());
		}
	}

	public void clear()
	{
		lines.clear();
		notifyDataSetChanged();
	}
	
	public FieldsLVAdapter(Context context) {
		this.context = context;
		lines=new ArrayList<>();

	}

	public void setCode(InputStream is) throws IOException
	{
		lines.clear();
		BufferedReader reader
			= new BufferedReader(new InputStreamReader(is));
		String line = reader.readLine();
		while(line != null){
			lines.add(line);
			line = reader.readLine();

		}

		notifyDataSetChanged();
	}



	@Override
	public int getCount() {
		return lines.size();
	}

	@Override
	public Object getItem(int position) {
		return lines.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {


		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) 
				context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(
				R.layout.op, null);
		}

		TextView name = (TextView)convertView.findViewById(R.id.opTV);
		
		String op=(String) getItem(position);
		if(op.equals(""))
			convertView.setVisibility(View.GONE);
		else
			convertView.setVisibility(View.VISIBLE);
		name.setText(lines.get(position));

		return convertView;
	}
}
