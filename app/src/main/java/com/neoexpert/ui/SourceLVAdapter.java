package com.neoexpert.ui;

import android.content.*;
import android.graphics.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import com.neoexpert.jvm.*;
import java.io.*;
import java.util.*;

public class SourceLVAdapter extends BaseAdapter
{
	private Context context;
	ArrayList<String> lines;
	WeakHashMap<String,ArrayList<String>> cashe=new WeakHashMap<>();
	private int pc;

	private int green=Color.parseColor("#009900");

	public void noCode(String filename)
	{
		lines.clear();
		lines.add("no source file found: " + filename);
	}

	public void clear()
	{
		lines.clear();
		pc=0;
		notifyDataSetChanged();
	}
	public void setPc(int pc){
		this.pc=pc;
		notifyDataSetInvalidated();
	}



	public SourceLVAdapter(Context context) {
		this.context = context;
		lines=new ArrayList<>();

	}

	public void setCode(String classname, InputStream is) throws IOException
	{
		lines = cashe.get(classname);
		if (lines == null)
		{
			lines = new ArrayList<String>();
			lines.clear();
			BufferedReader reader
				= new BufferedReader(new InputStreamReader(is));
			String line = reader.readLine();
			while (line != null)
			{
				lines.add(line);
				line = reader.readLine();

			}
			cashe.put(classname,lines);
		}
		notifyDataSetChanged();
	}



	@Override
	public int getCount() {
		return lines.size();
	}

	@Override
	public Object getItem(int position) {
		return lines.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {


		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) 
				context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(
				R.layout.op, null);
		}

		TextView name = (TextView)convertView.findViewById(R.id.opTV);
		if(pc==position){
			name.setBackgroundColor(green);
			name.setTextColor(Color.BLACK);
		}
		else{
			name.setBackgroundColor(Color.BLACK);
			name.setTextColor(Color.WHITE);
		}
		String op=(String) getItem(position);
		if(op.equals(""))
			convertView.setVisibility(View.GONE);
		else
			convertView.setVisibility(View.VISIBLE);
		name.setText(lines.get(position));

		return convertView;
	}
}
