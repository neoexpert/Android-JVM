package com.neoexpert.ui;

import android.app.*;
import android.content.*;
import android.graphics.*;
import android.net.*;
import android.os.*;
import android.view.*;
import android.webkit.*;
import android.widget.*;
import android.widget.AdapterView.*;
import com.neoexpert.jvm.*;
import com.neoexpert.jvm._abstract.*;
import com.neoexpert.jvm.adapter.*;
import com.neoexpert.jvm.constants.*;
import com.neoexpert.jvm.method.*;
import java.io.*;
import java.util.*;
import java.util.zip.*;



public class MainActivity extends Activity
implements OnItemClickListener,OutputAdapter
{

	private SourceLVAdapter sourcea;

	private ZipFile srczip;

	private ListView fieldsLV;

	private FieldsLVAdapter fieldsa;

	@Override
	public void jvmdebug(String a)
	{
		if(BuildConfig.DEBUG)
			logln(a,true);
	}

	private AClass c;
	
	private MainActivity.FrameStackLVAdapter fsa;

	@Override
	public void onItemClick(AdapterView<?> p1, View p2, int p, long p4)
	{
		ClassItem constant=(ClassItem) classa.getItem(p);
		if(constant instanceof Constant)
		switch(constant.getTag()){
			case Constant.CClass:
				String classname=classa.getConstant(((Constant)constant).index1).str;
				try
				{
					
					c=com.neoexpert.jvm. Class.getClass(classname);
					loadClass(this.c);
				}
				catch (Exception e)
				{
					Toast.makeText(this,e.getMessage(),Toast.LENGTH_LONG).show();
				}
				break;
		}
	}

	private TextView tv;

	private ListView frameStackLV;
	private ListView stackLV;
	private ListView lvLV;
	private ListView sourceLV;
	
	private MainActivity.StackLVAdapter sa;

	private MyThread t;

	private ListView codeLV;
	private ListView classLV;
	private CodeLVAdapter ca;

	private MainActivity.LvLVAdapter lv;

	private static final int YOUR_RESULT_CODE = 42;

	private ClassLVAdapter classa;
	
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        
		setContentView(R.layout.main);
		String cp=Environment.getExternalStorageDirectory().getAbsolutePath()+"/neoVM";
		com.neoexpert.jvm.Class.classpath=cp;
		com.neoexpert.jvm.Class.addJar("rt.jar");
		com.neoexpert.jvm.Class.addJar("charsets.jar");
		try
		{
			//JVM.main(null);
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}
		try
		{
			 srczip = new ZipFile(cp + "/src.zip");
		}
		catch (IOException e)
		{}

		tv = (TextView)findViewById(R.id.tv);
		classLV=(ListView)findViewById(R.id.lvClass);
		registerForContextMenu(classLV);
		classa=new ClassLVAdapter(this);
		classLV.setOnItemClickListener(this);
		classLV.setAdapter(classa);
		Object[] s=new Object[]{};
		Stack<Frame> fs=new Stack<>();
		
		stackLV=(ListView)findViewById(R.id.lvStack);
		sa=new StackLVAdapter(this,s);
		stackLV.setAdapter(sa);
		
		frameStackLV=(ListView)findViewById(R.id.frameStackLV);
		fsa=new FrameStackLVAdapter(this,fs);
		frameStackLV.setAdapter(fsa);
		
		lvLV=(ListView)findViewById(R.id.lvlv);
		lv=new LvLVAdapter(this);
		lvLV.setAdapter(lv);
		
		codeLV=(ListView)findViewById(R.id.lvCode);
		ca=new CodeLVAdapter(this);
		codeLV.setAdapter(ca);
		
		sourceLV=(ListView)findViewById(R.id.source);
		sourcea=new SourceLVAdapter(this);
		sourceLV.setAdapter(sourcea);
		
		fieldsLV=(ListView)findViewById(R.id.fieldsLV);
		fieldsa=new FieldsLVAdapter(this);
		fieldsLV.setAdapter(fieldsa);
		
		//Class c= new Class("java/lang/Object");
		
		//t.run();
		PrintStream myStream = new PrintStream(System.out) {
			@Override
			public void print(String x) {
				log(x);
			}
			@Override
			public void println(String x) {
				logln(x,false);
			}
			@Override
			public void print(int x) {
				log(Integer.toString(x));
			}
			@Override
			public void println(int x) {
				print(x);
				lognewline();
			}
			@Override
			public void print(long x) {
				log(Long.toString(x));
			}
			@Override
			public void println(long x) {
				print(x);
				lognewline();
			}
			@Override
			public void print(boolean x) {
				log(Boolean.toString(x));
			}
			@Override
			public void println(boolean x) {
				print(x);
				lognewline();
			}
			@Override
			public void print(float x) {
				log(Float.toString(x));
			}
			@Override
			public void println(float x) {
				print(x);
				lognewline();
			}
			@Override
			public void print(char x) {
				log(Character.toString(x));
			}
			@Override
			public void println(char x) {
				print(x);
				lognewline();
			}
			@Override
			public void print(char[] x) {
				log(new String(x));
			}
			@Override
			public void println(char[] x) {
				print(x);
				lognewline();
			}
			@Override
			public void print(double x) {
				log(Double.toString(x));
			}
			@Override
			public void println(double x) {
				print(x);
				lognewline();
			}
			@Override
			public void print(Object x) {
				log(x.toString());
			}
			@Override
			public void println(Object x) {
				print(x);
				lognewline();
			}
		};
		//System.setOut(myStream);
		
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
	{
		// TODO: Implement this method
		switch (v.getId())
		{
			case R.id.lvClass:
				int id = 
					((AdapterView.AdapterContextMenuInfo)menuInfo).position;
				ClassItem c=(ClassItem) classa.getItem(id);
				switch (c.getTag())
				{
					case Constant.CMethodRef:
					
					menu.setHeaderTitle("");

					

					menu.add(Menu.NONE, 0, 0, "open class");
					menu.add(Menu.NONE, 1, 0, "code");
					break;
					case Constant.CFieldRef:
						menu.add(Menu.NONE, 0, 0, "open class");
						//menu.add(Menu.NONE, 1, i, "code");
						
						break;
									case Constant.CClass:
						menu.add(Menu.NONE, 0, 0, "constant pool");
						//menu.add(Menu.NONE, 1, i, "code");

						break;
					
				}
				break;

		}
		getClass();
	}

	@Override
	public boolean onContextItemSelected(MenuItem item)
	{
		int pos=((AdapterView.AdapterContextMenuInfo)item.getMenuInfo()).position;
		ClassItem c=(ClassItem) classa.getItem(pos);
		
		switch (item.getItemId())
		{
			case 0:
				/*
				Constant c1=classa.getConstant(c.index1);
				c1=classa.getConstant(c1.index1);
				try
				{
					this.c=Class.getClass(c1.str);
					loadClass(this.c);
				}
				catch (Exception e)
				{}
				return true;*/
			case 1:
				/*
				try
				{
					Method m=this.c.getMethod(c.getId());
					Dialog dialog = new Dialog(this);
					dialog.setContentView(R.layout.method);
					TextView txt = (TextView)dialog.findViewById(R.id.method_text);
					txt.setText(m.toString());
					dialog.show();
					//Toast.makeText(this,m.toString(),Toast.LENGTH_LONG).show();
				}
				catch (Exception e)
				{
					Toast.makeText(this,e.toString(),Toast.LENGTH_LONG).show();
				}

				return true;*/
			
		}
		return false;
	}
	
	void loadClass(AClass c){
		
		//c=com.neoexpert.jvm.Class.getClass("sun/reflect/Reflection");
		//c.clinit();
		
        
        ///*
		
		
		//*/
		
		//com.neoexpert.jvm.Class.getClass("sun/misc/Unsafe");
		
		//com.neoexpert.jvm.Class.getClass("java/lang/reflect/Modifier");
		//com.neoexpert.jvm.Class.getClass("sun/nio/cs/StandardCharsets",false);
		
		//com.neoexpert.jvm.Class.getClass("java/nio/Bits");
		//c=com.neoexpert.jvm.Class.getClass("java/io/FileSystem",false);
		//com.neoexpert.jvm.Class.getClass("java/lang/Class$Atomic");
		//com.neoexpert.jvm.Class.getClass("java/lang/ref/Reference");
		
		//c=com.neoexpert.jvm.Class.getClass("java/lang/System");
		//c=com.neoexpert.jvm.Class.getClass("java/io/BufferedInputStream",false);
		//c=com.neoexpert.jvm.Class.getClass("java/nio/Bits",false);
		
		c=com.neoexpert.jvm.Class.getClass("sun/reflect/Reflection",false);
		t=c.createInitMethod();
		//t=new MyThread(c,"main",this);
		//t.setMethodToRun("main([Ljava/lang/String;)V");
		

		this.cC=c;
		loadSource(cC);
		
		
		//if (c.hasMainMethod())
		{		
			try
			{
				
				//t.setMethodToRun("initializeSystemClass()V");
				//t.setMethodToRun("<clinit>()V");
				
				//t.setMethodToRun("main([Ljava/lang/String;)V");
				
				
				//t.run();
				
				
				//t.setMethodToRun("main([Ljava/lang/String;)V");
				//t.init();
				
				
				
				/*
				c=com.neoexpert.jvm.Class.getClass("java/lang/System");
				this.cC=c;
				loadSource(cC);
				t = new MyThread(c,"system",this);
				t.setMethodToRun("initializeSystemClass()V");
				*/
			}
			catch (Exception e)
			{
				throw new RuntimeException(t.getStackTrace(),e);
			}
			t.setFrameStackAdapter(fsa);
			fsa.notifyDataSetChanged();
			t.setStackAdapter(sa);
			sa.notifyDataSetChanged();
			t.setCodeAdapter(ca);
			ca.notifyDataSetChanged();
			ca.setPc(0);
			t.setLvAdapter(lv);
			lv.notifyDataSetChanged();
			t.setClassAdapter(classa);
			try
			{
				//t.run();
			}
			catch (Exception e)
			{}
			findViewById(R.id.methodLayout).setVisibility(View.VISIBLE);
			
		}
		//else{
			//findViewById(R.id.methodLayout).setVisibility(View.GONE);
			//log("no main method");
		//}
		
	}
	public String getMimeType( Uri uri) {
		String extension;

		//Check uri format to avoid null
		if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
			//If scheme is a content
			final MimeTypeMap mime = MimeTypeMap.getSingleton();
			extension = mime.getExtensionFromMimeType(getContentResolver().getType(uri));
		} else {
			//If scheme is a File
			//This will replace white spaces with %20 and also other special characters. This will avoid returning null values on file name with spaces and special characters.
			extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(uri.getPath())).toString());

		}

		return extension;
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		
		switch(requestCode){
			case YOUR_RESULT_CODE:
				if(data==null)
					return;
				com.neoexpert.jvm.Class.reset();
				MyThread.reset();
				JVM.init();
				Uri uri=data.getData();
				String type=getMimeType(uri);
				try
				{
					switch (type)
					{
						default:
							
							InputStream raw=getContentResolver().openInputStream(uri);
							c = new com.neoexpert.jvm.Class(raw,true);
					}
				}
				catch (Exception e)
				{
					jvmdebug(e.getMessage());
					return;
				}
				
				loadClass(c);
				int line=t.getCurrentLine();
				if(line>0)
					sourcea.setPc(line-1);
				Toast.makeText(this, "LINE: "+(t.getCurrentLine()-1), Toast.LENGTH_LONG).show();
				//log(c.toString());
				findViewById(R.id.d_controls).setVisibility(View.VISIBLE);
				break;
		}
	}
	
	public void showJVM(View v){
		findViewById(R.id.jvm).setVisibility(View.VISIBLE);
		findViewById(R.id.source).setVisibility(View.GONE);
	}
	public void showSource(View v){
		findViewById(R.id.jvm).setVisibility(View.GONE);
		findViewById(R.id.source).setVisibility(View.VISIBLE);
	}
	
	public void loadClassFile(View v){
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
		intent.setType("*/*");
		startActivityForResult(intent, YOUR_RESULT_CODE);
	}
	Runnable step =new Runnable(){

		@Override
		public void run()
		{

			step(null);
			synchronized (this)
			{
				this.notify();
			}

		}
	};
	
	public void stepOut(View v){
		final int stackDepth=t.getStackSize();
		Thread th= new Thread(new Runnable(){

				@Override
				public void run()
				{
				
					
					while (true)
					{

						if (t.getStackSize() < stackDepth)
							break;

//					try
//					{
//						Thread.sleep(100);
//					}
//					catch (InterruptedException e)
//					{}
						synchronized (step)
						{
							runOnUiThread(step);
							try
							{
								step.wait();
							}
							catch (InterruptedException e)
							{}
						}
					}
				}
			});
		th.start();
	}
	AClass cC;
	public void step(View v)
	{
	
		if (t != null)
			try{
			if (!t.debuggerStep()){
				Toast.makeText(this, "HALT", Toast.LENGTH_LONG).show();
				Stack<Frame> stack=t.getExceptionStack();
				while(!stack.isEmpty()){
					Toast.makeText(this,stack.pop().toString(), Toast.LENGTH_LONG).show();
				}
				return;
			}
			}
			catch(Exception e){
				throw new RuntimeException(t.getStackTrace(),e);
			}
		if(!t.getCurrentClass().equals(cC)){
			cC=t.getCurrentClass();
			loadSource(cC);
			fieldsa.clear();
			fieldsa.setFields(t);
		}
		int line=t.getCurrentLine()-1;
		
		if(line>=0){
			sourcea.setPc(line);
			int last = sourceLV.getLastVisiblePosition();  

			int first = sourceLV.getFirstVisiblePosition();
			if(line<first||line>last)
				sourceLV.setSelection(line);
		}
		//else step(v);
		//Toast.makeText(this, "LINE: "+t.getCurrentLine(), Toast.LENGTH_LONG).show();
	}

	private void loadSource(AClass cC)
	{
		String classname=cC.getName();
		if(classname.contains("$"))
			classname=classname.substring(0,classname.indexOf('$'));
		final File file = new File(com.neoexpert.jvm.Class.classpath, classname + ".java");
        if (file.exists())
            try {
                InputStream raw = new FileInputStream(file);
                sourcea.setCode(classname,raw);
				return;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
		ZipEntry e = srczip.getEntry(classname + ".java");
		if (e == null) {
			sourcea.noCode(classname+".java");
			//throw new RuntimeException(cC.getName() + " class not found");
			return;
		}
		int size = srczip.size();
		try
		{
			InputStream is = srczip.getInputStream(e);
		
			sourcea.setCode(classname,is);
		}catch (IOException ex)
		{}
		
	}
	
	public void lognewline(){
		tv.append("\n");
	}
	public void logln(String t,boolean debug){
		//tv.append(t);
		//lognewline();
	}
	public void log(String t){
		//tv.append(t);
	}
	
	public class LvLVAdapter extends BaseAdapter 
	implements LvAdapter
	{

		private Context context;

		private Object[] lv;
	
		

		public void clear()
		{
			lv=lv=new Object[]{};
			notifyDataSetChanged();
		}
		
		public void setLv(Frame f)
		{
			if(f!=null)
				this.lv=f.lv;
			else
				lv=new Object[]{};
			
			notifyDataSetChanged();
		}

		public LvLVAdapter(Context context) {
			this.context = context;
			lv=new Object[]{};

		}

		@Override
		public int getCount() {
			return lv.length;
		}

		@Override
		public Object getItem(int position) {
			return lv[position];
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) 
                    getSystemService(LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(
                    R.layout.op, null);
			}

			TextView name = (TextView)convertView.findViewById(R.id.opTV);
			name.setText(getItemId(position)+": "+getItem(position));

			return convertView;
		}
	}
	public class CodeLVAdapter extends BaseAdapter
	implements CodeAdapter
	{
		private Context context;
		ArrayList<String> ops;
		private int pc;

		private int green=Color.parseColor("#009900");

		public void clear()
		{
			ops.clear();
			pc=0;
			notifyDataSetChanged();
		}
		public void setPc(int pc){
			this.pc=pc;
			notifyDataSetInvalidated();
		}

	

		public CodeLVAdapter(Context context) {
			this.context = context;
			ops=new ArrayList<>();
			
		}

		public void setCode(Frame f)
		{
			ops.clear();
			int pc=0;
			if(f!=null)
			while(pc<f.code.length){
				StringBuilder cs = new StringBuilder();
				int l=CodeAttribute.parseOp(pc, f.code,cs);
				
				ops.add(cs.toString());
				for(int i=1;i<l;i++){
					ops.add("");
				}
				pc+=l;
			}
			
			notifyDataSetChanged();
		}

		

		@Override
		public int getCount() {
			return ops.size();
		}

		@Override
		public Object getItem(int position) {
			return ops.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {


			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) 
                    getSystemService(LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(
                    R.layout.op, null);
			}

			TextView name = (TextView)convertView.findViewById(R.id.opTV);
			if(pc==position){
				name.setBackgroundColor(green);
				name.setTextColor(Color.BLACK);
			}
			else{
				name.setBackgroundColor(Color.BLACK);
				name.setTextColor(Color.WHITE);
			}
			String op=(String) getItem(position);
			if(op.equals(""))
				convertView.setVisibility(View.GONE);
				else
					convertView.setVisibility(View.VISIBLE);
			name.setText(ops.get(position));
			
			return convertView;
		}
	}
	public class FrameStackLVAdapter extends BaseAdapter 
	implements FrameStackAdapter
	{

		private Context context;
		private Stack<Frame> stack;

		public FrameStackLVAdapter(Context context, Stack<Frame> stack) {
			this.context = context;
			this.stack = stack;
		}

		public void clear()
		{
			stack.clear();
			notifyDataSetChanged();
		}

		public void setStack(Stack<Frame> stack)
		{
			this.stack=stack;
			notifyDataSetChanged();
		}

		@Override
		public int getCount() {
			return stack.size();
		}

		@Override
		public Object getItem(int position) {
			return stack.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {


			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) 
                    getSystemService(LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(
                    R.layout.op, null);
			}

			TextView name = (TextView)convertView.findViewById(R.id.opTV);

			name.setText(getItem(position).toString());
			if(position==stack.size()-1)
				name.setTextColor(Color.GREEN);
			else
				name.setTextColor(Color.LTGRAY);
			return convertView;
		}
	}
	public class StackLVAdapter extends BaseAdapter 
	implements StackAdapter
	{

		private Context context;
		private Object[] stack;

		public StackLVAdapter(Context context, Object[] stack) {
			this.context = context;
			this.stack = stack;
		}

		public void clear()
		{
			notifyDataSetChanged();
		}

		public void setFrame(Frame f)
		{
			if(f!=null)
				stack=f.stack;
			else
				stack=new Object[]{};
			notifyDataSetChanged();
		}

		@Override
		public int getCount() {
			return stack.length;
		}

		@Override
		public Object getItem(int position) {
			return stack[position];
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {


			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) 
                    getSystemService(LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(
                    R.layout.op, null);
			}

			TextView name = (TextView)convertView.findViewById(R.id.opTV);
			
			name.setText(getItem(position)+"");
			
			return convertView;
		}
	}
	
}
