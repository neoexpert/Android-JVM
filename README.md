# Android-JVM
Simulates a JVM on Android


APP:
https://play.google.com/store/apps/details?id=com.neoexpert.jvm


## Example code
Example code that does run after compilation to a class file:

```java
public class Main {
	public static void main(String[] args) {
		fak(10);
	}
	public static int fak(int i){
		if(i<=0)
			return 1;
		return i*fak(i-1);
	}
}
```
